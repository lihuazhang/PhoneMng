<!DOCTYPE html>
<%@ include file="/WEB-INF/views/commons/include.jsp"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ page language="java" pageEncoding="UTF-8"%>
<style type="text/css">
    #index_banner_top {
        background: #35abd9 none repeat scroll 0 0;
        height: auto;
    }
    .row{
       margin-left:-15px;
       margin-right:-15px; 
       margin-top:50px;   
    }
    .section {
	    padding-bottom: 50px;
	    padding-top: 50px;
    } 
</style>
<script type="text/javascript">
    $(document).ready(function() {
        $().UItoTop({
            easingType : 'easeOutQuart'
        });

        $(".scroll").click(function(event) {
            event.preventDefault();
            $('html,body').animate({
                scrollTop : $(this.hash).offset().top
            }, 1000);
        });
        jQuery('#index_banner').camera({
            height: '350px',
            thumbnails : true,
            barPosition:"bottom"
        });

    });
    
     $.ajaxSetup({
         contentType:"application/x-www-form-urlencoded;charset=utf-8",   
         complete:function(XMLHttpRequest,textStatus){ 
           var sessionstatus=XMLHttpRequest.getResponseHeader("sessionstatus"); 
               if(sessionstatus=="timeout"){ 
                   alert("登录超时,请重新登录！");
                   //如果超时就处理 ，指定要跳转的页面  
                 window.location.replace(webPath.webRoot + "account/login");   
               }   
            }   
       });
    
</script>

</head>
<body role="document">

    <%@include file="/WEB-INF/views/commons/header.jsp"%>
    <div style="height:350px">
    <div class="camera_wrap camera_azure_skin" id="index_banner">
        <div data-thumb="${ctx}/images/main-1.png"
            data-src="${ctx}/images/main-1.png">
        </div>
        <div data-thumb="${ctx}/images/main-2.png"
            data-src="${ctx}/images/main-2.png">
        </div>
        <div data-thumb="${ctx}/images/main-3.png"
            data-src="${ctx}/images/main-3.png">
        </div>

        <div data-thumb="${ctx}/images/main-4.png"
            data-src="${ctx}/images/main-4.png">
        </div>
    </div>
    </div>
    <!---content--->
    <div class="content">
        <!---content-we-are--->
        <div class="content-we-are" id="team">
            <!---container--->
            <div class="container">
                    <div class="clearfix"></div>
                    <!---- End ---->
                </div>
            </div>
        </div>
        <!---container--->
         <div class="container">
        <div class="text-center margin-top-100">
            <div class="title">选择适合的Native APP测试套餐开始测试</div>
            <div class="text-sub">我们为您提供不同维度的测试套餐为你解决测试的烦恼</div>
        </div>
        <div class="row clearfix">
            <div class="package col-xs-4">
                <div class="card text-center">
                    <div class="border-top bt1"></div>
                    <div class="icon"><img src="${ctx}/images/icon1.png" alt="" /></div>
                    <div class="name">性能测试</div>
                    <div class="time">预计用时4小时</div>
                    <hr>
                    <div></div>
                    <div class="text-sub">应用流量耗用性能</div>
                    <div class="text-sub">应用能量耗用性能</div>
                    <div class="text-sub">应用CPU、RAM性能</div>
                </div>
            </div>
            <div class="package col-xs-4">
                <div class="card text-center">
                    <div class="border-top bt2"></div>
                    <div class="icon"><img src="${ctx}/images/icon2.png" alt="" /></div>
                    <div class="name">兼容测试</div>
                    <div class="time">预计用时4小时</div>
                    <hr>
                    <div></div>
                    <div class="text-sub">应用安装、卸载测试</div>
                    <div class="text-sub">应用执行测试</div>
                    <div class="text-sub">ＵＩ遍历测试</div>
                </div>
            </div>
            <div class="package col-xs-4">
                <div class="card text-center">
                    <div class="border-top bt3"></div>
                    <div class="icon"><img src="${ctx}/images/icon3.png" alt="" /></div>
                    <div class="name">功能测试</div>
                    <div class="time">预计用时4小时</div>
                    <hr>
                    <div></div>
                    <div class="text-sub">功能遍历测试</div>
                    <div class="text-sub">自定义脚本测试</div>
                    <div class="text-sub">录制回放</div>
                </div>
            </div>
            <div class="package col-xs-4">
                <div class="card text-center">
                    <div class="border-top bt4"></div>
                    <div class="icon"><img src="${ctx}/images/icon4.png" alt="" /></div>
                    <div class="name">网络友好测试</div>
                    <div class="time">预计用时4小时</div>
                    <hr>
                    <div></div>
                    <div class="text-sub">数据包深度分析</div>
                    <div class="text-sub">网络友好综合评价</div>
                    <div class="text-sub">网络友好优化建设</div>
                </div>
            </div>
            <div class="package col-xs-4">
                <div class="card text-center">
                    <div class="border-top bt5"></div>
                    <div class="icon"><img src="${ctx}/images/icon5.png" alt="" /></div>
                    <div class="name">弱网络测试</div>
                    <div class="time">预计用时4小时</div>
                    <hr>
                    <div></div>
                    <div class="text-sub">软件弱网络模拟</div>
                    <div class="text-sub">真实网络衰减</div>
                
                </div>
            </div>
            <div class="package col-xs-4">
                <div class="card text-center">
                    <div class="border-top bt6"></div>
                    <div class="icon"><img src="${ctx}/images/icon6.png" alt="" /></div>
                    <div class="name">专机测试</div>
                    <div class="time">预计用时4小时</div>
                    <hr>
                    <div></div>
                    <div class="text-sub">针对最新上市机型</div>
                    <div class="text-sub">Ａｐｐ兼容性测试</div>
                    <div class="text-sub">Ａｐｐ性能测试</div>
                </div>
            </div>
         </div>
     </div>    
<div class="section" style="background:#f9f9f9;">
	<div class="container">
		<div class="text-center">
			<div class="title">更多服务</div>
			<div>我们提供更多的服务来满足你的需要</div>
		</div>
		<div class="row">
			<div class="col-xs-6 service">
				<div class="box">
					<div class="bg bg-1"></div>
						<div class="content">
							<div class="name">Web App测试服务</div>
							<div class="desc">支持Web App性能及功能测试，输入移动URL即可自动分析并定位性能瓶颈和不同分辨率手机上的兼容性问题</div>
							<a href="#" class="start">点击体验</a>
						</div>
					</div>
				</div>
			<div class="col-xs-6 service">
				<div class="box">
					<div class="bg bg-3"></div>
						<div class="content">
							<div class="name">线上监控服务</div>
							<div class="desc">可实时监控移动应用发布后的线上渠道、性能及崩溃问题等，为线上产品质量保驾护航</div>
							<a href="#" class="start">点击体验</a>
						</div>
					</div>
				</div>
			</div>
	</div>
</div>        
<div class="bottom"> &copy; 2014.Phonemng |Phonemng</div>
</body>
</html>