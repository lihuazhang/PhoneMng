<%@ page language="java" pageEncoding="UTF-8"%>

<script type="text/javascript">

	$(document).ready(function() {
		
		$("#registerBtn").on("click", function () {
			window.location.href = "${ctx}/biz/user/registerUser";
            console.log("Register BTN click.");
        });
		
		function randomNumber(min, max) {
            return Math.floor(Math.random() * (max - min + 1) + min);
        };
        
        $('#captchaOperation').html([randomNumber(1, 50), '+', randomNumber(1, 99), '='].join(' '));
		
        $('#loginForm').bootstrapValidator({
            message: '输入数据非法.',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                email: {
                    validators: {
                        emailAddress: {
                            message: 'Email地址非法.'
                        },
                        notEmpty: {
                            message: 'Email不能为空.'
                        },
                    }
                },
                password: {
                    validators: {
                        notEmpty: {
                            message: '密码不能为空.'
                        }
                    }
                },
                captcha: {
                    validators: {
                        callback: {
                            message: '答案错误',
                            callback: function(value, validator) {
                                var items = $('#captchaOperation').html().split(' '), sum = parseInt(items[0]) + parseInt(items[2]);
                                return value == sum;
                            }
                        }
                    }
                }
            }
        });
        
		 $('#resetBtn').click(function() {
             $('#loginForm').data('bootstrapValidator').resetForm(true);
         });
	    
	});
	
	$(function() {
		$('#loginModal').on('show.bs.modal', function(e) {
			console.log("lllgin login login login.");
			
		});

	});
</script>
<!---header--->
<div class="header">
	<!---container--->
	<div class="container">
		<div class="top-header">
			 <div class="logo">
				<a href="#"><img src="${ctx}/images/smartium_logo.png" /></a>
			</div>  
			<div class="menu">
				<a class="toggleMenu" href="#"><img src="${ctx}/images/nav-icon.png" alt="" /> </a>
				<ul class="nav" id="nav">
					<li class="active"><a href="${ctx}/">首页</a></li>
					<li><a href="${ctx}/#services" class="scroll">设备租用</a>
					   <ul class="dropdown-menu">
                                <li><a href="${ctx}/biz/device">设备浏览</a></li>
                                <hr  style="border:1px solid #033951;align:center;" class="border" >
                                <li>
                                    <a href="${ctx}/biz/device/sigleDevcePage.htm">真机操作</a>
                                </li>
                                <hr  style="border:1px solid #033951;align:center;" class="border" >
                                <li>
                                    <a href="app-test.html?id=3">功能测试</a>
                                </li>
                                <hr  style="border:1px solid #033951;align:center;" class="border" >
                                <li>
                                    <a href="app-test.html?id=4">性能测试</a>
                                </li>
                                <hr  style="border:1px solid #033951;align:center;" class="border" >
                                <li>
                                    <a href="intro_netFriendly.html">网络友好测试</a>
                                </li>
                                <hr  style="border:1px solid #033951;align:center;" class="border" >
                                <li>
                                    <a href="intro_weakNet.html">弱网络测试</a>
                                </li>
                                <hr  style="border:1px solid #033951;align:center;" class="border" >
                                <li style="margin-bottom:5px">
                                    <a target="_blank"  href="intro_specialTest.html">专机测试</a>
                                </li>
                            </ul>
					</li>
					<li><a href="${ctx}/biz/test/testMainPage.htm" >自动化测试</a></li>
					<li><a href="#" >我的报告</a></li>
					<li><a href="${ctx}/#contact" class="scroll">联系我们</a></li>
					 <c:choose>
						 <c:when test="${empty user}">
			                <li><a href="${ctx}/biz/user/loginview" data-login-id="login-id"
	                        data-toggle="modal">${not empty user?user.userName:'登录/注册'}</a></li>
			            </c:when>
			            <c:otherwise>
			               <li><a data-toggle="dropdown" href="#" class="dropdown-toggle">
                               <span> <small>欢迎,</small> ${not empty user?user.userName:'登录/注册'}
                                  <span class="glyphicon glyphicon-user"></span>
                               </span> <span class="caret"></span> </a>
		                        <ul class="dropdown-menu">
		                            <li><a href="${ctx}/login"> ${not empty user?user.userName:'登录/注册'}
		                                    <span class="glyphicon glyphicon-user"></span></a>
		                            </li>
		                            <li><a href="${ctx}/biz/log"><span
		                                    class="glyphicon glyphicon-cog">设置</span></a></li>
		                            <li><a href="#"><span class="glyphicon glyphicon-user">编辑</span></a>
		                            </li>
		                            <li role="separator" class="divider"></li>
		                            <li><a href="#"><span class="glyphicon glyphicon-off">退出</span></a>
		                            </li>
		                        </ul>
                          </li>
			            </c:otherwise>
					</c:choose>
					
				</ul>

			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<!---container--->
</div>
<!---header--->

<div class="modal fade" id="loginModal" tabindex="-1" draggable="true"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" >
		<div class="modal-content">
			<div class="modal-header" style="color:#fff;background-color:#c0c7ba !important">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title">登录</h4>
			</div>
		<div class="modal-body" style="color:#fff;background-color:#c0c7ba !important" >
		<div class="container">
        <div class="row">
            <!-- form: -->
            <section>
                <div class="col-lg-8">
                    <form id="loginForm" method="post" modelAttribute="user" class="form-horizontal" action="${ctx}/login">
                       
                        <div class="form-group">
                            <label class="col-lg-3 control-label">邮件</label>
                            <div class="col-lg-5">
                                <input type="text" class="form-control" name="email" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">密码</label>
                            <div class="col-lg-5">
                                <input type="password" class="form-control" name="password" />
                            </div>
                        </div>

                       
                        <div class="form-group">
                            <label class="col-lg-3 control-label" id="captchaOperation"></label>
                            <div class="col-lg-2">
                                <input type="text" class="form-control" name="captcha" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-9 col-lg-offset-3">
                                <button type="submit" class="btn btn-primary" name="signup" value="Sign up">登录</button>
                                <!-- <button type="button" class="btn btn-info" id="resetBtn">重置</button> -->
                                <a href="${ctx}/biz/user/registerUser" class="btn btn-info">注册</a>
                               <!--  <button type="button" class="btn btn-default" data-dismiss="modal">取消</button> -->
                            </div>
                        </div>
                    </form>
                </div>
            </section>
            <!-- :form -->
        </div>
    </div>
			</div>
			<!-- modalbody end -->
			<div class="modal-footer" style="background-color:#c0c7ba !important">
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal -->
</div>