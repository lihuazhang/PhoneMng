<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="es" tagdir="/WEB-INF/tags" %>
<%@ page language="java" pageEncoding="UTF-8"%>

<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<link rel="shortcut icon" href="${ctx}/images/favicon.ico" type="image/x-icon">


<link rel="stylesheet"  type="text/css"  href="${ctx}/css/bootstrap-theme.css"/>
<link rel="stylesheet"  type="text/css"  href="${ctx}/css/bootstrap.css"/>
<link rel="stylesheet"  type="text/css"  href="${ctx}/css/phmng.css"/>
<link rel="stylesheet"  type="text/css"  href="${ctx}/css/camera.css"/>
<link rel="stylesheet"  type="text/css"  href="${ctx}/css/bootstrapValidator.css"/>
<link rel="stylesheet"  type="text/css"  href="${ctx}/css/device-search-style.css"/>

<script type="text/javascript" src="${ctx}/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="${ctx}/js/bootstrap.js"></script>

<script type="text/javascript" src="${ctx}/js/index/easing.js"></script>
<script type="text/javascript" src="${ctx}/js/index/move-top.js"></script>
<script type="text/javascript" src="${ctx}/js/index/responsive-nav.js"></script>
<script type="text/javascript" src="${ctx}/js/index/responsiveslides.min.js"></script>
<script type="text/javascript" src="${ctx}/js/index/camera.min.js"></script>
<script type="text/javascript" src="${ctx}/js/phonemng.js"></script>
<script type="text/javascript" src="${ctx}/js/bootstrapValidator.js"></script>


<title>PhoneMNG</title>


