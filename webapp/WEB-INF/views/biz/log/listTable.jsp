
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/commons/include.jsp"%>
<table id="table" class="sort-table table table-bordered table-hover" data-async="true">
    <thead>
    <tr>
       
        <th style="width: 100px" sort="id">编号</th>
        <th style="width: 150px" sort="loginIp">IP</th>
        <th>URL</th>
        <th>登录日期</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${page.content}" var="log">
        <tr>
            <td>
                <a class="btn btn-link btn-edit" href="${ctx}/biz/log/${log.id}">${log.id}</a>
            </td>
            <td>${log.loginIp}</td>
            <td>${log.requestUrl}</td>
            <td>${log.created_date}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<es:page page="${page}"/>
