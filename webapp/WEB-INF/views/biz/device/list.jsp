<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/commons/include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
<!--
.block {
	weight: 270px;
	border: 1px solid rgba(174, 174, 174, 0.32);
}

.blue-font {
	padding-left: 20px;
	padding-top: 40px;
	font-size: 12px;
	color: #3879D9;
}

.font-black {
	padding-left: 20px;
	padding-top: 10px;
	font-size: 12px;
}
-->
</style>
<script language="javascript" type="text/javascript">
	jQuery(document).ready(function() {
		$("#bt_display1").on('click', function() {
			$("#my1").toggle();
		});
		$("#bt_display2").on('click', function() {
			$("#my2").toggle();
		});
		$("#bt_display3").on('click', function() {
			$("#my3").toggle();
		});
	});
</script>
</head>
<body role="document">
	<%@include file="/WEB-INF/views/commons/header.jsp"%>
	<div id="container">
		<div id="device-table">
			<div data-table="table" class="select-panel _selectPanel collapsed">
			    <div class="select-panel-b">
			        <form class="breadcrumb form-search" id="searchForm" modelAttribute="device" action="#" method="get" style="background-color: rgba(255, 255, 255, 0.45);">
						 <div class="os select-b clearfix" style="padding-bottom: 0px !important;" >				     
						 	<h2>系统版本:</h2>
					         <div class="select-c _osSelB">
					             <label>
					                 <input name="system" value="5.0.1" type="checkbox">
					                 <span>5.0.1</span>
					             </label>
					             <label>
						             <input name="system" value="5.0" type="checkbox">
						             <span>5.0</span>
					             </label>
					             <label>
						             <input name="system" value="4.4.4" type="checkbox">
						             <span>4.4.4</span>
					             </label>
					             <label>
						             <input name="system" value="4.4.2" type="checkbox">
						             <span>4.4.2</span>
					             </label>
					             <label>
						             <input name="system" value="4.3" type="checkbox">
						             <span>4.3</span>
					             </label>
					             <label>
						             <input name="system" value="4.2.2" type="checkbox">
						             <span>4.2.2</span>
					             </label>
					             <label>
						             <input name="system" value="4.1.2" type="checkbox">
						             <span>4.1.2</span>
					             </label>
					             <label>
						             <input name="system" value="3.2.0" type="checkbox">
						             <span>3.2.0</span>
					             </label>
					             <label>
						             <input name="system" value="3.0.2" type="checkbox">
						             <span>3.0.2</span>
					             </label>
				             </div>
						 </div>

					     <div class="brand select-b clearfix" style="padding-bottom: 0px !important;">
					         <h2>品牌:</h2>
						     <div class="select-c brand-c _brandSelB">
						     <label>
							     <input name="brand" value="三星" type="checkbox">
							     <span>三星</span>
						     </label>
						     <label>
							     <input name="brand" value="LG" type="checkbox">
							     <span>LG</span>
						     </label>
						     <label>
							     <input name="brand" value="酷派" type="checkbox">
							     <span>酷派</span>
						     </label>
						     <label>
							     <input name="brand" value="索尼" type="checkbox">
							     <span>索尼</span>
						     </label>
						     <label>
							     <input name="brand" value="谷歌" type="checkbox">
							     <span>谷歌</span>
						     </label>
						     <label>
							     <input name="brand" value="OPPO" type="checkbox">
							     <span>OPPO</span>
						     </label>
						     <label>
							     <input name="brand" value="纽曼" type="checkbox">
							     <span>纽曼</span>
						     </label>
						     <label>
							     <input name="brand" value="华硕" type="checkbox">
							     <span>华硕</span>
						     </label>
						     </div>
					     </div>
					     <div class="resolution select-b clearfix" style="padding-bottom: 0px !important;">
					     <h2>分辨率:</h2>
						     <div class="select-c resolution-c _resSelB">
						     <label>
							     <input name="resolution-ratio" value="720x1280" type="checkbox">
							     <span>720x1280</span>
						     </label>
						     <label>
							     <input name="resolution-ratio" value="540x960" type="checkbox">
							     <span>540x960</span>
						     </label>
						     <label>
							     <input name="resolution-ratio" value="1440x2560" type="checkbox">
							     <span>1440x2560</span>
						     </label>
						     <label>
							     <input name="resolution-ratio" value="1080x1920" type="checkbox">
							     <span>1080x1920</span>
						     </label>
						     </div>
					     </div>
					     <div class="hot select-b clearfix" style="padding-bottom: 0px !important;">
					     <h2>设备状态:</h2>
						     <div class="select-c hot-c _hotSelB">
						     <label>
							     <input name="type" value="available" type="checkbox">
							     <span>可使用</span>
						     </label>
						     <label>
							     <input name="type" value="ordered" type="checkbox">
							     <span>已预约</span>
						     </label>
						     </div>
					     
					</form>
					<span class="coll _collBtn">更多选项V</span>
				</div>
			</div>
				<%@include file="listTable.jsp"%>
			<div style="padding-left: 20px !important;">
            <es:page page="${page}" />
            </div>
        </div>
   </div>
		<!-- end device-table -->
		
	</div>
</body>
</html>