
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/commons/include.jsp"%>

<div id="device-table-product">
	<tbody>
		<c:forEach items="${page.content}" var="device">

			<!--  <form name="frmApply" id="frmApply" action="http://remote.utest.qq.com/control" method="post" target="_blank">
              -->
			<div class="product">
				<div class="product-title">
					<h2>${device.brand}</h2>
				</div>
				<div class="product-content">
					<img class="img-rounded" src="${ctx}/images/device/sam.png" alt=""> 
					<div>
						<div><span class="span-style1">品牌：</span><span class="span-style2">${device.brand}</span></div>
						<div style="padding-top:10px"><span class="span-style1">系统：</span><span class="span-style2">Android</span></div>
						<div style="padding-top:10px"><span class="span-style1">版本：</span><span class="span-style2"> 4.2.2</span></div>
						<div style="padding-top:10px"><span class="span-style1">分辨率：</span><span class="span-style2">${device.resolution}</span></div>
					</div>
				</div>

				<c:if test="${device.status eq 'FREE' }">
					<div class="product-button-online">
						<button
							onclick="return window.location.href='${ctx}/biz/device/sigleDevcePage.htm?id=${device.id}';">申请租用</button>
					</div>
				</c:if>
				<c:if test="${device.status eq 'ONLINE' }">
					<div class="product-button-online">
						<button
							onclick="return window.location.href='${ctx}/biz/device/sigleDevcePage.htm?id=${device.id}';">申请租用</button>
					</div>
				</c:if>
				<c:if test="${device.status eq 'BUSY' }">
					<div class="product-button-busy">
						<button
							onclick="#">设备繁忙</button>
					</div>
				</c:if>
				<c:if test="${device.status eq 'OFFLINE' }">
					<div class="product-button-offline">
						<button onclick="#">设备下线</button>
					</div>
				</c:if>

			</div>
			<!-- product div end -->
		</c:forEach>
	</tbody>

</div>




