<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/commons/include.jsp"%>
<!DOCTYPE html>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link rel="stylesheet" type="text/css"
	href="${ctx}/css/remote-control.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/css/siglePhone.css" />

<script type="text/javascript"
	src="${ctx}/js/device/deviceRemoteControl.js"></script>
<script type="text/javascript" src="${ctx}/js/device/excanvas.js"></script>
<script type="text/javascript" src="${ctx}/js/device/screenshot.js"></script>
<script type="text/javascript" src="${ctx}/js/device/sockjs-1.0.3.js"></script>
<script type="text/javascript" src="${ctx}/js/device/show.js"></script>
<script type="text/javascript" src="${ctx}/js/device/showDivTier.js"></script>
<script type="text/javascript" src="${ctx}/js/device/rightbar-common.js"></script>



<style type="text/css">
</style>

</head>
<body>
	<div id="loadbar" style="padding-top: 20%; display: none;">
		<img src="${ctx}/images/device/initLoading2.gif"
			style="z-index: 1050;">
		<div style="margin-top: 0px;">
			<span
				style="font-size: 18px; color: #FFFFFF; position: absolute; top: 463px; left: 612px;">手机初始化中,请耐心等待...</span>
		</div>
	</div>
	<%@include file="/WEB-INF/views/commons/header.jsp"%>
	<!-- 开始内嵌页面 -->
	<div id="container">
	    <div class="deviceHead">
	    <div class="deviceName ng-binding">三星 SM-N9108V 4.4.4</div>
	    <div class="timeToUse ng-scope ng-binding">剩余时间：<span id="counter"></span></div>
	    </div>
	</div>
	<div id="container">
		<div class="clear-both"></div>
		<div class="content">
			<div class="center">
				<div id="content-left">
					<div class="slide-left">
						<div id="well" class="well">
							<div id="device-wraper" style="width: 337px;">

								<canvas id="deviceCanvas" width="337" height="600"> 
								<h1>Canvas not supported.推荐使用最新版chrome浏览器</h1></canvas>
							</div>
						</div>
						<div id="button-kit">
							<span><button id="home" class="home_btn" value="home">
									<img src="${ctx}/images/device/home.png"><span>Home</span>
								</button></span> <span><button id="menu" class="menu_btn" value="menu">
									<img src="${ctx}/images/device/menu.png">Menu
								</button></span> <span><button id="back" class="back_btn" value="back">
									<img src="${ctx}/images/device/back.png">Back
								</button></span> <span><button id="search" class="search_btn"
									value="search">
									<img src="${ctx}/images/device/search.png">Search
								</button></span>
						</div>
					</div>
				</div>
				<div id="content-right">
					<div class="slider-right">
						<div id="slider-right-header">
							<h1>
								<span id="mobile-title"
									style="border-bottom: 2px solid #4186C9;">控制面板</span> 
								<span id="mutidevcie-title">多设备显示</span>
							</h1>
						</div>

						<div id="tab-device-op">
							<hr>
							<div id="box-status">
							<div class="deviceInfo">
							    <span class="font-style">系统版本：4.4.4</span>
							    <span class="font-style">品牌：三星</span>
							    <span class="font-style">型号：SM-N9108V</span>
							    <span class="font-style">分辨率：1440x2560</span>
							</div>
							<div style="clear:both" >
							        <div class="deviceControl">
							        <span class="box-style">
									<span class="ico_capture"></span>
									<span class="ico_text">截图</span>
									</span>
									<span class="box-style">
									<span class="ico_capture"></span>
									<span class="ico_text">旋转</span>
									</span>
									<span class="box-style">
									<span class="ico_upload"></span>
									<span class="ico_text">上传应用</span>
									</span>
									<span class="box-style">
									<span class="ico_exit"></span>
									<span class="ico_text">退出</span>
									</span>
									</div>
                            </div>
                            <div class="deviceLogTitle">Logcat:</div>
                            <div class="deviceLog">
                                <div class="stf-scope ng-scope stf-logs ng-scope">
                                    <div class="widget-container">
                                        <div class="widget-content">
                                            <table class="table table-condensed logcat-filters-table">
                                                <tbody>
                                                    <tr>
                                                        <td width=7%>
                                                            <button class="btn btn-xs btn-primary-outline ng-pristine ng-untouched ng-valid" style="height: 32px;width: 92px">
                                                                <i class="fa fa-list-alt"></i>
                                                                <span class="ng-binding ng-scope">开始记录</span>
                                                            </button>
                                                        </td>
                                                        <td width=8%>
                                                            <select class="ng-pristine ng-untouched ng-valid" style="padding-top: 10px;">
															    <option class="ng-binding" value="0" disabled="disabled" selected="selected">level</option>
															    <option label="Verbose" value="1">Verbose</option>
															    <option label="Debug" value="2">Debug</option>
															    <option label="Info" value="3">Info</option>
															    <option label="Warn" value="4">Warn</option>
															    <option selected="selected" label="Error" value="5">Error</option>
															    <option label="Fatal" value="6">Fatal</option>
														    </select>
                                                        </td>
                                                        <td width="10%">
													        <input placeholder="时间" class="input-sm form-control ng-pristine ng-untouched ng-valid" type="text">
													    </td>
													    <td width="28%">
													        <input placeholder="内容" class="input-sm form-control ng-pristine ng-untouched ng-valid" type="text">
													    </td>
													    <td width="5%">
														    <button class="btn-xs btn btn-sm btn-danger-outline pull-right ng-isolate-scope" style="height: 32px;width: 92px">
															    <i class="fa fa-trash-o"></i>
															    <span translate="translate">
															        <span class="ng-scope">清除</span>
															    </span>
														    </button>
													    </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="stf-logcat-table force-gpu">
											    <table class="console-message-text tableX table-condensed selectable">
												    <tbody>
												    </tbody>
											    </table>
										    </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           
							<div id="box-operation" style="display:none">
								<table cellpadding="0" cellspacing="0">
									<thead>
										<tr>
											<th colspan="5">手机操作</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="border-right"><span id="attachdiv">
													<button id="attach" name="attach" class="start"
														data-loading-text="上传中..." data-hover="tooltip"
														data-original-title="apk上传限制250M!">
														<img src="${ctx}/images/device/apk-install.png">
														安装APK
													</button>
											</span>
												<form id="uploadForm" method="post" style="display: none;">
													<input type="file" name="file" accept=".apk"
														id="fileupload" data-url="#"><br>
												</form></td>
											<td class="border-right">
												<button id="cutscreen">
													<img src="${ctx}/images/device/screen-cut.png">屏幕截图
												</button>
											</td>
										</tr>
										<tr>
											<td class="border-right">
												<div>
													<label class="col-lg-3 control-label">命令</label>
													<div class="col-lg-5">
														<input type="text" class="form-control" name="nickName" />
													</div>
												</div>
											</td>
											<td class="border-right">
												<div class="form-group">
													<label class="col-lg-3 control-label">Shell</label>
													<div class="col-lg-5">
														<input type="text" class="form-control" name="Shell" />
													</div>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<!-- device info end -->


						<!-- tab-mutidevcie-info end -->
					</div>
				        
						<div id="tab-mutidevcie-info" style="display: none;">
							<hr>
							<div  class="box-info">
								<div class="mutidevice">
									<div class="product-title">
										<h2>设备1</h2>
									</div>
								</div>
								<div class="mutidevice">
                                    <div class="product-title">
                                        <h2>设备1</h2>
                                    </div>
                                </div>
							</div>
						</div>
				</div>
			</div>
		</div>
	</div>

	<div class="mui-mbar-tabs">
		<div class="quick_link_mian">
			<div class="quick_links_panel">
				<div id="quick_links" class="quick_links">

					<li id="shopCart"><a href="#" class="message_list"><i
							class="message"></i>
							<div class="span">机器选择</div> <span class="cart_num">0</span></a></li>

				</div>

			</div>
			<div id="quick_links_pop" class="quick_links_pop hide"></div>
		</div>
	</div>
	<script>
		//el
		//var staticPath = "http://cdn.utest.qq.com/remote/";
		var did = 76;
		var startLogcatUrl = "http://localhost:8080/PhoneMng/remoteAjax/startLogcat?id=76";
		var id = ${id};
		var ctx = "http://localhost:8080/PhoneMng";

		var usageId = "421252";
		var imageProvider = "";
		//剩余时间
		var minToWait = "15"; // wait 30 min
		var secToWait = "0";
		//超时时间毫秒级
		var totalTimeOut = "10";
		var deviceHostIp = "192.168.20.208";
		var userId = "51339a289b1445228747c3b00cc9b966";
		var use_type = "0";
		var canvasTag = document.getElementById('deviceCanvas');
		if (!canvasTag.getContext) {
			G_vmlCanvasManager.initElement(canvasTag);
		}
		var context = canvasTag.getContext('2d');
		var imgSize = 0.5;//默认为 一半大小
		//--pic设置DEFAULT放大缩小率  by shawn
		var pRate = 1.0;
		//var xRate = 0.01;//横坐标图像拉伸比例
		//var yRate = 0.01;//纵坐标图像拉伸比例
		var xRes = 480;//录制时屏幕分辨率中的x轴大小
		var yRes = 854;//录制时屏幕分辨率中的y轴大小   
		//创建一个新的透明图层  by shawn-start
		var canvasDiv = document.getElementById('device-wraper');
		var canvas = document.createElement('canvas');
		if (!canvas.getContext) {
			G_vmlCanvasManager.initElement(canvas);
		}
		var canvasWidth = 480, canvasHeight = 800;
		var point = {};
		point.notFirst = false;
		var deviceCanvas = document.getElementById('deviceCanvas');//得到屏幕的id
		canvasWidth = deviceCanvas.getAttribute('width');
		canvasHeight = deviceCanvas.getAttribute('height');
		console.log("canvasWidth=" + canvasWidth + ",canvasHeight="
				+ canvasHeight);
		canvas.setAttribute('width', canvasWidth);
		canvas.setAttribute('height', canvasHeight);
		var device_wraper = document.getElementById('device-wraper');
		canvas.setAttribute('style', 'cursor:pointer;position:absolute;left:'
				+ getLeft(device_wraper) + ';top:' + getTop(device_wraper)
				+ ';z-index:100;');
		canvas.setAttribute('id', 'canvas');
		canvasDiv.insertBefore(canvas, deviceCanvas);
		if (typeof G_vmlCanvasManager != 'undefined') {
			canvas = G_vmlCanvasManager.initElement(canvas);
		}
		var kpl_context = canvas.getContext("2d");
		//shawn-end
		var onbeforeunload_handler_url = "http://localhost:8080/remoteAjax/lazyReleaseControl?id=421252";
		var onbeforeunload_handler_rd_flag_url = "http://localhost:8080/remoteAjax/lazyReleaseRemoteDebug?token=";
		var checkRemoteDebug_param = "";
	</script>
</body>
</html>