<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/commons/include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

</head>
<body role="document">
	<%@include file="/WEB-INF/views/commons/header.jsp"%>
	<form class="breadcrumb form-search" id="searchForm"
		modelAttribute="plan" action="planList.htm" method="get">
		
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}" />
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}" /> 
		
		<div class="checkbox">
		   <div class="row">
		      <div class="col-xs-1">
                  <label>OS</label>
              </div>
		      <div class="col-md-4">
		          <label><input type="checkbox" value="">Andriod</label>
		      </div>
		   </div>
		   
		</div>
		<div class="checkbox">
		    <div class="row">
              <div class="col-xs-1">
                  <label>分辨率</label>
              </div>
              <div class="col-md-4">
                  <label><input type="checkbox" value="">1280*1080</label>
              </div>
           </div>
		</div>
		<div class="checkbox">
            <div class="row">
              <div class="col-xs-1">
                  <label>品牌</label>
              </div>
              <div class="col-md-4">
                  <label><input type="checkbox" value="">XiaoMi</label>
              </div>
           </div>
        </div>
        <div class="checkbox">
            <div class="row">
              <div class="col-xs-1">
                  <label>状态</label>
              </div>
              <div class="col-md-4">
                  <label><input type="checkbox" value="">空闲</label>
              </div>
           </div>
        </div>
		
		<div class="radio">
		   <div class="row">
              <div class="col-xs-1">
                  <label>选项</label>
              </div>
              <div class="col-md-4">
                    <label>
		              <input type="radio" name="optionsRadios" id="optionsRadios2" 
		                 value="option2">选项 2 - 选择它将会取消选择选项 1
		           </label>
              </div>
           </div>
		  
		</div>
		<hr>
		<div class="radio">
		     <div class="row">
              <div class="col-xs-1">
                  <label>类型</label>
              </div>
              <div class="col-md-4">
                     <label class="checkbox-inline">
		              <input type="checkbox" id="inlineCheckbox1" value="option1">手机
		           </label>
		           <label class="checkbox-inline">
		              <input type="checkbox" id="inlineCheckbox2" value="option2">平板
		           </label>
		           <label class="checkbox-inline">
		              <input type="checkbox" id="inlineCheckbox3" value="option3">电视盒
		           </label>
		           <label class="checkbox-inline">
		              <input type="radio" name="optionsRadiosinline" id="optionsRadios3" 
		                 value="option1" checked>智能电视
		           </label>
              </div>
           </div>
		</div>
		
	</form>
</body>
</html>