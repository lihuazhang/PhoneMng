<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/commons/include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style type="text/css">
body {
	-webkit-background-size: cover;
	-moz-background-size: cover;
	-o-background-size: cover;
	background-size: cover;
	color: #fff;
	font-family: 'microsoft yahei', Arial, sans-serif;
}
</style>
</head>
<body role="document">
	<%@include file="/WEB-INF/views/commons/header.jsp"%>
	<hr>
	<div class="container" role="main">
	  
		<div class="row">
			<div class="col-md-3 easy-them">
                <div class="servicebox">
                    <div class="servicebox-icon icon01"></div>
                    <div class="servicebox-content">
                        <h3 style="color: black;">真机列表</h3>
                        <a href="${ctx}/biz/device"><p>
                                <span class="glyphicon glyphicon-ok"> 设备浏览</span>
                            </P>
                            <p>
                                <span class="glyphicon glyphicon-ok"> 真机访问</span>
                            </p>
                            <p>
                                <span class="glyphicon glyphicon-ok"> APK安装</span>
                            </p> </a>
                        </p>
                    </div>
                </div>
            </div><!-- div end -->
			
			<div class="col-md-3 easy-them">
                <div class="servicebox">
                    <div class="servicebox-icon icon01"></div>
                    <div class="servicebox-content">
                        <h3 style="color: black;">远程真机访问</h3>
                        <a href="${ctx}/biz/device/sigleDevcePage.htm"><p>
                                <span class="glyphicon glyphicon-ok"> 真机访问</span>
                            </P>
                            <p>
                                <span class="glyphicon glyphicon-ok"> 应用能量耗用性能</span>
                            </p>
                            <p>
                                <span class="glyphicon glyphicon-ok"> 应用CPU、RAM性能</span>
                            </p> </a>
                        </p>
                    </div>
                </div>
            </div><!-- div end -->
            
			<div class="col-md-3 easy-them">
				<div class="servicebox">
					<div class="servicebox-icon icon01"></div>
					<div class="servicebox-content">
						<h3 style="color: black;">性能测试</h3>
						<a href="#"><p>
								<span class="glyphicon glyphicon-ok"> 应用流量耗用性能</span>
							</P>
							<p>
								<span class="glyphicon glyphicon-ok"> 应用能量耗用性能</span>
							</p>
							<p>
								<span class="glyphicon glyphicon-ok"> 应用CPU、RAM性能</span>
							</p> </a>
						</p>
					</div>
				</div>
			</div>

		</div>
	</div>
	<!-- main container -->
</body>
</html>