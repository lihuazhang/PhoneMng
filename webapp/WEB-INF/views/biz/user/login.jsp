<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/commons/include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<style>
<!--
.help-block {
    display: block;
    margin-top: 10px;
    margin-left: 67px;
}
.has-success .control-label { color: rgba(0, 0, 0, 0.73);}
-->
</style>
<head>
<link rel="stylesheet" type="text/css" href="${ctx}/css/login.css" />
</head>

<body class="login">
 <%@include file="/WEB-INF/views/commons/header.jsp"%>
 <div class="container" style="border-top: 1px solid #EBECED;padding-bottom: 60px;">
 <div class="main-photo">
     <img src="${ctx}/images/main-photo.jpg">
 </div>
	<div class="content">
		<form id="login_form" action="${ctx}/login" modelAttribute="user" method="post" class="form-vertical login-form" style="display: block;" >
			<h3 class="form-title">SmartCloud账号登录</h3>
			
			<div class="control-group form-group">
				<label class="control-label visible-ie8 visible-ie9">输入账号:</label>
				<span class="controls">
					<span class="input-icon left">
					    <input class="m-wrap placeholder-no-fix" type="text" id="email" name="email">
					</span>
				</span>
			</div>
			<div class="control-group form-group">
				<label class="control-label visible-ie8 visible-ie9">输入密码:</label>
				<span class="controls">
					<span class="input-icon left">
						<i class="fa fa-lock"></i> <input class="m-wrap placeholder-no-fix" type="password"
							id="password" name="password">
					</span>
				</span>
			</div>

			<div class="form-actions">
				<label class="checkbox">
					<div class="checker">
						<span class="checked"><input type="checkbox" name="remember" value="1"></span>
					</div>下次自动登录</label>
				<button id="login_submit_btn" type="submit"
					class="btn blue pull-right">登录 <i class="m-icon-swapright m-icon-white"></i>
				</button>

			</div>

			<div class="create-account">
				<p>
					<a href="javascript:;" class="" id="forget-password">忘记密码 ?</a><a
						href="${ctx}/biz/user/registerUser" id="" class="">创建账户</a>
				</p>
			</div>

		</form>

		<form class="form-vertical forget-form" method="post" action="#"
			style="display: none;">
			<h3 class="">忘记密码 ?</h3>
			<p>请输入您的注册邮箱.</p>
			<div class="control-group">
				<div class="controls">
					<div class="input-icon left">
						<i class="fa fa-envelope"></i> <input
							class="m-wrap placeholder-no-fix" type="text" placeholder="Email"
							name="email">
					</div>
				</div>
			</div>

			<div class="form-actions">
				<button type="button" id="back-btn" class="btn">
					<i class="m-icon-swapleft"></i> 返回</button>
				<button type="submit" class="btn blue pull-right">
					提交 <i class="m-icon-swapright m-icon-white"></i>
				</button>
			</div>
		</form>

		<form class="form-vertical register-form"
			action="${ctx}/biz/user/registerUser" novalidate="novalidate">
			<h3 class="">创建账户</h3>
			<div class="control-group">
				<label class="control-label visible-ie8 visible-ie9">账号</label>
				<div class="controls">
					<div class="input-icon left">

						<i class="fa fa-user"></i> <input
							class="m-wrap placeholder-no-fix" type="text" placeholder="账号"
							name="username">

					</div>

				</div>

			</div>

			<div class="control-group">
				<label class="control-label visible-ie8 visible-ie9">密码</label>
				<div class="controls">
					<div class="input-icon left">
						<i class="fa fa-lock"></i> <input
							class="m-wrap placeholder-no-fix" type="password"
							id="register_password" placeholder="密码" name="password">
					</div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label visible-ie8 visible-ie9">重复密码</label>
				<div class="controls">
					<div class="input-icon left">

						<i class="fa fa-check-square-o"></i> <input
							class="m-wrap placeholder-no-fix" type="password"
							placeholder="重复密码" name="rpassword">
					</div>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label visible-ie8 visible-ie9">Email</label>
				<div class="controls">
					<div class="input-icon left">
						<i class="fa fa-envelope"></i> <input
							class="m-wrap placeholder-no-fix" type="text" placeholder="Email"
							name="email">
					</div>
				</div>
			</div>

			<div class="form-actions">
				<button id="register-back-btn" type="button" class="btn">
					<i class="m-icon-swapleft"></i> 返回
				</button>

				<button type="submit" id="register-submit-btn"
					class="btn blue pull-right">
					注册 <i class="m-icon-swapright m-icon-white"></i>
				</button>

			</div>

		</form>

	</div>
    
</div>
<div class="container" >
<div style="border-top: 1px solid #EBECED;padding-top:20px"></div>
<div class="copyright">2015 © SmartCloud</div>
</div>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			function randomNumber(min, max) {
	            return Math.floor(Math.random() * (max - min + 1) + min);
	        };
	        $('#captchaOperation').html([randomNumber(1, 50), '+', randomNumber(1, 99), '='].join(' '));
	         
			Login.init();
		});
		
		var Login = function() {

			return {
				//main function to initiate the module
				init : function() {
					$('#login_form').bootstrapValidator({
		                message: '输入数据非法.',
		                feedbackIcons: {
		                    valid: 'glyphicon glyphicon-ok',
		                    invalid: 'glyphicon glyphicon-remove',
		                    validating: 'glyphicon glyphicon-refresh'
		                },
		                fields: {
		                    email: {
		                        validators: {
		                            emailAddress: {
		                                message: 'Email地址非法.'
		                            },
		                            notEmpty: {
		                                message: '账号不能为空.'
		                            },
		                        }
		                    },
		                    password: {
		                        validators: {
		                            notEmpty: {
		                                message: '密码不能为空.'
		                            },
		                            stringLength: {
		                                min: 5,
		                                max: 30,
		                                message: '密码输入在6-30之间.'
		                            }
		                        }
		                    },
		                    captcha: {
		                        validators: {
		                            callback: {
		                                message: '答案错误',
		                                callback: function(value, validator) {
		                                    var items = $('#captchaOperation').html().split(' '), sum = parseInt(items[0]) + parseInt(items[2]);
		                                    return value == sum;
		                                }
		                            }
		                        }
		                    }
		                }
		            });
					$('.login-form input').keypress(function(e) {
						if (e.which == 13) {
							jQuery("#login_submit_btn").trigger("click");
							return false;
						}
					});

					$('.forget-form input').keypress(function(e) {
						if (e.which == 13) {
							if ($('.forget-form').validate().form()) {
								$(".forget-form").ajaxSubmit();
							}
							//return false;
						}
					});

					jQuery('#forget-password').click(function() {
						jQuery('.login-form').hide();
						jQuery('.forget-form').show();
					});

					jQuery('#back-btn').click(function() {
						jQuery('.login-form').show();
						jQuery('.forget-form').hide();
					});

					jQuery('#register-btn').click(function() {
						jQuery('.login-form').hide();
						jQuery('.register-form').show();
					});

					jQuery('#register-back-btn').click(function() {
						jQuery('.login-form').show();
						jQuery('.register-form').hide();
					});

					
				}

			};

		}();
	</script>
</body>
</html>