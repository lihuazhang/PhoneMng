<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/commons/include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="${ctx}/css/login.css" />
<style type="text/css">
   body {
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
    color:#fff;
    font-family: 'microsoft yahei' ,Arial,sans-serif;
}
.has-success .form-control{border-color: rgba(164, 164, 164, 0.7);}
.has-success .form-control:focus { 
	box-shadow: 0px 1px 1px rgba(123, 123, 123, 0.1) inset, 0px 0px 6px rgba(164, 164, 164, 0.7);
	border-color: rgba(153, 153, 153, 0.7);
}
.has-success .control-label { color: rgba(0, 0, 0, 0.73);}
</style>
<script type="text/javascript">
   $(document).ready(function() {
       $('#regBack').click(function() {
           window.location.href = "${ctx}/";
       });
        
         function randomNumber(min, max) {
                return Math.floor(Math.random() * (max - min + 1) + min);
            };
         $('#captcha').html([randomNumber(1, 100), '+', randomNumber(1, 200), '='].join(' '));
        
        $('#userForm').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                userName: {
                     message: 'The username is not valid',
                     validators: {
                         notEmpty: {
                             message: '用户名不能为空！'
                         },
                         stringLength: {
                             min: 6,
                             max: 30,
                             message: '用户名输入在6-30之间.'
                         },
                         regexp: {
                             regexp: /^[a-zA-Z0-9_\.]+$/,
                             message: 'The username can only consist of alphabetical, number, dot and underscore'
                         },
                         different: {
                             field: 'password,confirmPassword',
                             message: '密码不能和姓名相同.'
                         }
                     }
                 },
                email: {
                    validators: {
                        notEmpty: {
                            message: 'Email不能为空.'
                        },
                        emailAddress: {
                            message: 'Email地址非法.'
                        }
                    }
                },
                password: {
                    validators: {
                        notEmpty: {
                            message: '密码不能为空.'
                        },
                        identical: {
                            field: 'confirmPassword',
                            message: '密码不一致.'
                        },
                        different: {
                            field: 'userName',
                            message: '密码不能和姓名相同.'
                        }
                    }
                },
                confirmPassword: {
                    validators: {
                        notEmpty: {
                            message: '确认密码不能为空.'
                        },
                        identical: {
                            field: 'password',
                            message: '密码不一致.'
                        },
                        different: {
                            field: 'userName',
                            message: 'The password cannot be the same as username'
                        }
                    }
                },
                captcha: {
                    validators: {
                        callback: {
                            message: '答案错误',
                            callback: function(value, validator) {
                                var items = $('#captcha').html().split(' '), sum = parseInt(items[0]) + parseInt(items[2]);
                                return value == sum;
                            }
                        }
                    }
                }
            }
        });

        $('#resetButton').click(function() {
            $('#userForm').data('bootstrapValidator').resetForm(true);
        });
        
    });
</script>

</head>
<body  class="login">
     
    <%@include file="/WEB-INF/views/commons/header.jsp"%>
    
     <div class="container" style="border-top: 1px solid #EBECED;padding-bottom: 60px;">
         <div class="main-photo">
             <img src="${ctx}/images/main-photo.jpg">
         </div>
         <div class="content">
            <!-- form: -->
            <section>
                    <div class="page-header font-color-black">
                        <h3>新用户注册</h3>
                    </div>

                    <form id="userForm" method="post" modelAttribute="user"  class="form-horizontal" action="${ctx}/biz/user/submitRegUser.htm">
                        <div class="form-group font-color-black">
                            <label class="col-lg-4 control-label register-font-style">姓名</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control register-form-style" name="userName" />
                            </div>
                        </div>
                         <div class="form-group font-color-black">
                            <label class="col-lg-4 control-label register-font-style">昵称</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control register-form-style" name="nickName" />
                            </div>
                        </div>

                        <div class="form-group font-color-black">
                            <label class="col-lg-4 control-label register-font-style">邮件</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control register-form-style" name="email" />
                            </div>
                        </div>

                        <div class="form-group font-color-black">
                            <label class="col-lg-4 control-label register-font-style">密码</label>
                            <div class="col-lg-8">
                                <input type="password" class="form-control register-form-style" name="password" />
                            </div>
                        </div>

                        <div class="form-group font-color-black">
                            <label class="col-lg-4 control-label register-font-style">再次输入密码</label>
                            <div class="col-lg-8">
                                <input type="password" class="form-control register-form-style" name="confirmPassword" />
                            </div>
                        </div>

                        <div class="form-group font-color-black">
                            <label class="col-lg-4 control-label register-font-style" id="captcha"></label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control register-form-style" name="captcha" />
                            </div>
                        </div>

                        <div class="form-group font-color-black">
                            <div class="col-lg-8 col-lg-offset-4">
                                <button type="submit" class="btn btn-primary" name="signup" value="Sign up">注册</button>
                                <!-- <button type="button" class="btn btn-info" id="resetButton">重置</button> -->
                                <button type="button" class="btn btn-success" name="regBack" id="regBack">返回</button>
                            </div>
                        </div>
                    </form>
            </section>
            <!-- :form -->
            </div>
    </div>
    <div class="container" >
        <div style="border-top: 1px solid #EBECED;padding-top:20px"></div>
        <div class="copyright">2015 © SmartCloud</div>
    </div>
    <%-- <div class="backstretch"
        style="left: 0px; top: 0px; overflow: hidden; margin: 0px; padding: 0px; height: 799px; width: 1364px; z-index: -999999; position: fixed;">
        <img src="${ctx}/images/bg4.jpg"
            style="position: absolute; margin: 0px; padding: 0px; border: none; width: 1364px; height: 723.155172413793px; max-width: none; z-index: -999999; left: 0px; top: -62.0775862068966px;">
    </div> --%>
</body>
</html>