var ctx="PhoneMng/";
var myctx="/PhoneMng/biz/device";
var clickX = new Array();
var clickY = new Array();
var clickDrag = new Array();
var actionArray = new Array();
var pressed = false;
var actionNumber = 0;
var errorAlertFlag = true;
var initFlag = false;
var imgIfValidFlag = false;
var loadingIfCloseFlag = false;
var logcatSwitchFlg = false;
var rotateRate = 0;
var resize = 2;
var defaultResize = 2;
var millionTime = 0;
var beginTime = 0;
var endTime = 0;
var record = false;
var record_pause = false;
var record_begin = false;
var rowid = 1;
var xbegin;
var ybegin;
var xend;
var yend;
var lastsl;
var KEYCODE_BACK = 4;
var KEYCODE_HOME = 3;
var KEYCODE_MENU = 82;
var KEYCODE_SEARCH = 84;
var KEYCODE_POWER = 26;
var KEYCODE_CLEAR = 28;
var KEYCODE_VOLUME_UP = 24;
var KEYCODE_VOLUME_DOWN = 25;
var KEYCODE_CAMERA = 27;
var KEYCODE_0 = 7;
var KEYCODE_1 = 8;
var KEYCODE_2 = 9;
var KEYCODE_3 = 10;
var KEYCODE_4 = 11;
var KEYCODE_5 = 12;
var KEYCODE_6 = 13;
var KEYCODE_7 = 14;
var KEYCODE_8 = 15;
var KEYCODE_9 = 16;
var KEYCODE_STAR = 17;
var KEYCODE_POUND = 18;
var KEYCODE_APAD_UP = 19;
var KEYCODE_APAD_DOWN = 20;
var KEYCODE_APAD_LEFT = 21;
var KEYCODE_APAD_RIGHT = 22;
var KEYCODE_APAD_CENTER = 23;
var KEYCODE_A = 29;
var KEYCODE_B = 30;
var KEYCODE_C = 31;
var KEYCODE_D = 32;
var KEYCODE_E = 33;
var KEYCODE_F = 34;
var KEYCODE_G = 35;
var KEYCODE_H = 36;
var KEYCODE_I = 37;
var KEYCODE_J = 38;
var KEYCODE_K = 39;
var KEYCODE_L = 40;
var KEYCODE_M = 41;
var KEYCODE_N = 42;
var KEYCODE_O = 43;
var KEYCODE_P = 44;
var KEYCODE_Q = 45;
var KEYCODE_R = 46;
var KEYCODE_S = 47;
var KEYCODE_T = 48;
var KEYCODE_U = 49;
var KEYCODE_V = 50;
var KEYCODE_W = 51;
var KEYCODE_X = 52;
var KEYCODE_Y = 53;
var KEYCODE_Z = 54;
var KEYCODE_COMMA = 55;
var KEYCODE_PERIOD = 56;
var KEYCODE_SPACE = 62;
var KEYCODE_DEL = 67;
var KEYCODE_GRAVE = 68;
var KEYCODE_MINUS = 69;
var KEYCODE_EQUALS = 70;
var KEYCODE_LEFT_BRACKET = 71;
var KEYCODE_RIGHT_BRACKET = 72;
var KEYCODE_BACKSLASH = 73;
var KEYCODE_SEMICOLON = 74;
var KEYCODE_APOSTROPHE = 75;
var KEYCODE_SLASH = 76;
var KEYCODE_AT = 77;
function dealKeyPress(a) {
	var b = 0;
	if (a >= 48 && a <= 57) {
		b = a - 41
	} else {
		if (a >= 97 && a <= 122) {
			b = a - 68
		} else {
			switch (a) {
			case 42:
				b = KEYCODE_STAR;
				break;
			case 35:
				b = KEYCODE_POUND;
				break;
			case 44:
				b = KEYCODE_COMMA;
				break;
			case 46:
				b = KEYCODE_PERIOD;
				break;
			case 32:
				b = KEYCODE_SPACE;
				break;
			case 96:
				b = KEYCODE_GRAVE;
				break;
			case 45:
				b = KEYCODE_MINUS;
				break;
			case 61:
				b = KEYCODE_EQUALS;
				break;
			case 91:
				b = KEYCODE_LEFT_BRACKET;
				break;
			case 93:
				b = KEYCODE_RIGHT_BRACKET;
				break;
			case 92:
				b = KEYCODE_BACKSLASH;
				break;
			case 59:
				b = KEYCODE_SEMICOLON;
				break;
			case 39:
				b = KEYCODE_APOSTROPHE;
				break;
			case 47:
				b = KEYCODE_SLASH;
				break;
			case 64:
				b = KEYCODE_AT;
				break;
			default:
				return
			}
		}
	}
	if (b != 0) {
		keyEvent(0, b);
		keyEvent(1, b)
	}
}
function dealKeyDown(a) {
	var b = 0;
	switch (a) {
	case 38:
		b = KEYCODE_APAD_UP;
		break;
	case 40:
		b = KEYCODE_APAD_DOWN;
		break;
	case 37:
		b = KEYCODE_APAD_LEFT;
		break;
	case 39:
		b = KEYCODE_APAD_RIGHT;
		break;
	case 13:
		b = KEYCODE_APAD_CENTER;
		break;
	case 8:
		b = KEYCODE_DEL;
		break;
	default:
		return
	}
	if (b != 0) {
		keyEvent(0, b);
		keyEvent(1, b)
	}
}

$(document).ready(function() {
					if (!window.WebSocket) {
						alert("FATAL: WebSocket not natively supported. This demo will not work!");
						window.close();
						return
					}
					window.onbeforeunload = a;
					function a() {
						$.ajax({
							url : onbeforeunload_handler_url,
							async : false
						});
						if ($("#rd_flag").length > 0) {
							$.ajax({
								url : onbeforeunload_handler_rd_flag_url,
								async : false
							})
						}
					};
					
				});
var images = [];
var ws;
var objectUrl;
var wsImgurl;
var wsLogCat;
var wsSendEvent;
$(document).ready(
		function() {
			showBg();
			initPage();
			//initRR();
			initSaveFile();
			countDownTimer();
			countDown = setInterval(countDownTimer, 1000);
			if ($("#rd_flag").length > 0) {
				setInterval(function() {
					checkRemoteDebug(checkRemoteDebug_param)
				}, 5000)
			}
			adjustCanvasSize(xRes, yRes);
			images[0] = new Image();
			wsImgurl = "ws://" + window.location.host
					+myctx+ "/wsServlet/getImage?id=" + id;
			if ('WebSocket' in window) {
				ws = new WebSocket(wsImgurl);//WebSocket;SockJS
				console.log("===WebSocket==");
			}else if ('MozWebSocket' in window) {
                websocket = new MozWebSocket(wsImgurl);
                console.log("===MozWebSocket==");
            } else {
                websocket = new SockJS(wsImgurl);
                console.log("===SockJS==");
            }
			
			if (ws != null) {
				ws.binaryType = "blob";
				ws.onopen = function() {
					console.log("[WebSocket#onopen] and send id="+id);
					ws.send(id);
					setInterval(loopWsImage, 6000)
				};
				ws.onclose = function() {
					console.log("[WebSocket#onclose]");
					ws = null
				};
				ws.onerror = function(evt) {
					console.log("getImage Error!"+evt.data);
					
					if (errorAlertFlag) {
						errorAlertFlag = false;
						alert("Connect phone error, please check.")
					}
					ws = null
				};
				ws.onmessage = function(e) {
					images[0].onload = function() {
						if (initFlag && !loadingIfCloseFlag) {
							closeBg();
							loadingIfCloseFlag = true
						}
						if (rotateRate % 2) {
							context.drawImage(this, 0, 0, canvasTag.height,
									canvasTag.width)
						} else {
							context.drawImage(this, 0, 0, canvasTag.width,
									canvasTag.height)
						}
						context.fillStyle = "#f00";
						window.URL.revokeObjectURL(images[0].src)
					};
					images[0].onerror = function() {
						console.log("image error")
					};
					
					/*if (window.navigator.userAgent.indexOf("Chrome") >= 1 || window.navigator.userAgent.indexOf("Safari") >= 1) {
						objectUrl= window.URL.createObjectURL(e.data);
					}else{
						objectUrl = window.URL.createObjectURL(e.data);
					}
						
					images[0].src = objectUrl;*/
					objectUrl = window.URL.createObjectURL(e.data);
					images[0].src = objectUrl
					
				}
			}
			var wsLogUrl = "ws://" + window.location.host
					+ myctx+"/wsServlet/getLogCat?id=" + id + "&totalTimeOut="
					+ totalTimeOut + "&userId=" + userId + "&usageId=" + usageId;
			
			
			if (wsLogCat != null) {
				wsLogCat.onopen = function() {
					console.log("[wsLogCat#onopen]");
					setInterval(loopWsLogCat, 9000)
				};
				wsLogCat.onclose = function() {
					console.log("[wsLogCat#onclose]");
					wsLogCat = null
				};
				wsLogCat.onerror = function(evt) {
					console.log("wsLogCat#Error!");
					if (errorAlertFlag) {
						errorAlertFlag = false;
						alert("NetWork error,please refresh explor!")
						console.log("网络传输异常,请重新刷新浏览器!");
					}
					wsLogCat = null
				};
				wsLogCat.onmessage = function(e) {
					var logs = eval(e.data);
					if (logcatSwitchFlg) {
						addRow(logs)
					}
				}
			}
			var wsSendEventUrl = "ws://" + window.location.host
					+ myctx+"/wsServlet/sendEvent?id=" + id + "&totalTimeOut="
					+ totalTimeOut + "&usageId=" + usageId;
			
			if ('WebSocket' in window) {
				wsSendEvent = new WebSocket(wsSendEventUrl);
				console.log("===sendEvent WebSocket==");
			}else if ('MozWebSocket' in window) {
				wsSendEvent = new WebSocket(wsSendEventUrl);
                console.log("=== sendEvent MozWebSocket==");
            } else {
            	wsSendEvent = new SockJS(wsSendEventUrl);
                console.log("===sendEvent SockJS==");
            }
			if (wsSendEvent != null) {
				wsSendEvent.onopen = function() {
					console.log("[wsSendEvent#onopen]");
					setInterval(loopWsSendEvent, 9000)
				};
				wsSendEvent.onclose = function() {
					console.log("[wsSendEvent#onclose]");
					wsSendEvent = null
				};
				wsSendEvent.onerror = function(evt) {
					console.log("wsSendEvent#Error!");
					if (errorAlertFlag) {
						errorAlertFlag = false;
						alert("NetWork error,please refresh explor.")
					}
					wsSendEvent = null
				};
				wsSendEvent.onmessage = function(e) {
					console.log("send event and got response="+e.data);
				}
			}
			ajaxInitDevice();
		});
function loopWsImage() {
	if (ws == null) {
		return
	}
	ws.send("loop")
}
function loopWsLogCat() {
	if (wsLogCat == null) {
		return
	}
	wsLogCat.send("loop")
}
function loopWsSendEvent() {
	if (wsSendEvent == null) {
		return
	}
	wsSendEvent.send("loop")
}
$("document").ready(function() {
	$("#attach").tooltip("hide");
	$("#tab-logcat-info").hide();
	$("#mobile-title").attr("style", "border-bottom:3px solid #4186C9;color:#0000ff;");
	$("#mobile-title").click(function() {
		$("#tab-logcat-info").hide();
		$("#tab-mydevice-info").hide();
		$("#tab-device-op").show();
		$("#tab-mutidevcie-info").hide();
		$("#mobile-title").attr("style", "border-bottom:3px solid #4186C9;color:#0000ff;");
		$("#logcat-title").attr("style", "border-bottom:0;");
		$("#deviceInfo-title").attr("style", "border-bottom:0;");
		$("#mutidevcie-title").attr("style", "border-bottom:0;")
	});
	$("#logcat-title").click(function() {
		$("#tab-device-op").hide();
		$("#tab-mydevice-info").hide();
		$("#tab-logcat-info").show();
		$("#tab-mutidevcie-info").hide();
		$("#logcat-title").attr("style", "border-bottom:3px solid #4186C9;color:#0000ff;");
		$("#mobile-title").attr("style", "border-bottom:0;");
		$("#deviceInfo-title").attr("style", "border-bottom:0;");
		$("#mutidevcie-title").attr("style", "border-bottom:0;")
	});
	$("#deviceInfo-title").click(function() {
		$("#tab-device-op").hide();
		$("#tab-logcat-info").hide();
		$("#tab-mydevice-info").show();
		$("#tab-mutidevcie-info").hide();
		$("#deviceInfo-title").attr("style", "border-bottom:3px solid #4186C9;color:#0000ff;");
		$("#logcat-title").attr("style", "border-bottom:0;");
		$("#mobile-title").attr("style", "border-bottom:0;");
		$("#mutidevcie-title").attr("style", "border-bottom:0;")
	});
	$("#mutidevcie-title").click(function() {
		$("#tab-device-op").hide();
		$("#tab-logcat-info").hide();
		$("#tab-mydevice-info").hide();
		$("#tab-mutidevcie-info").show();
		$("#mutidevcie-title").attr("style", "border-bottom:3px solid #4186C9;color:#0000ff;");
		$("#deviceInfo-title").attr("style", "border-bottom:0;");
		$("#logcat-title").attr("style", "border-bottom:0;")
		$("#mobile-title").attr("style", "border-bottom:0;")
	})
});

$(window).keypress(function(b) {
	var a = b.srcElement || b.target;
	if (null != a && (a.type == "text" || a.type == "textarea")) {
		return
	}
	dealKeyPress(b.which)
});

$(window).keydown(function(b) {
	var a = b.srcElement || b.target;
	if (null != a && a.type == "text") {
		return
	}
	dealKeyDown(b.which);
});

function addActionNumber() {
	if (actionNumber > 9) {
		actionNumber = 0
	} else {
		actionNumber++
	}
}

function redraw() {
	context.strokeStyle = "#0000ff";
	context.lineJoin = "round";
	context.lineWidth = 3;
	for (var a = 0; a < clickX.length; a++) {
		context.beginPath();
		if (clickDrag[a] && a) {
			context.moveTo(clickX[a - 1], clickY[a - 1])
		}
		context.lineTo(clickX[a], clickY[a]);
		context.closePath();
		context.stroke()
	}
}

function initPage() {
	var c;
	function a() {
		kpl_context.strokeStyle = "red";
		kpl_context.lineJoin = "round";
		kpl_context.lineWidth = 20;
		while (clickX.length > 0) {
			point.bx = point.x;
			point.by = point.y;
			point.x = clickX.pop();
			point.y = clickY.pop();
			point.drag = clickDrag.pop();
			kpl_context.beginPath();
			if (point.drag && point.notFirst) {
				kpl_context.moveTo(point.bx, point.by)
			} else {
				point.notFirst = true;
				kpl_context.moveTo(point.x - 1, point.y)
			}
			kpl_context.lineTo(point.x, point.y);
			kpl_context.closePath();
			kpl_context.stroke()
		}
	}
	$("#canvas").mousedown(function(g) {
		var f = g.pageX - this.offsetLeft;
		var d = g.pageY - this.offsetTop;
		c = true;
		addClick(g.pageX - this.offsetLeft, g.pageY - this.offsetTop);
		a();
		pressed = true;
		addActionNumber();
		beginTime = g.timeStamp;
		console.log("x="+(g.pageX - this.offsetLeft)+",y="+(g.pageY - this.offsetTop));
		console.log("mousedown!");
		pointEvent(g.pageX - this.offsetLeft, g.pageY - this.offsetTop, "0")
	});
	$("#canvas").mousemove(
			function(g) {
				if (c) {
					addClick(g.pageX - this.offsetLeft, g.pageY
							- this.offsetTop, true);
					a()
				}
				if (2 == use_type) {
					return
				}
				if (pressed) {
					var f = g.pageX - this.offsetLeft;
					var d = g.pageY - this.offsetTop;
					addClick(g.pageX - this.offsetLeft, g.pageY
							- this.offsetTop, true);
					if (record && !record_pause) {
						actionArray.push({
							Xpoint : f,
							Ypoint : d,
							actualaction : 2
						})
					}
					console.log("mousemove!");
					pointEvent(g.pageX - this.offsetLeft, g.pageY
							- this.offsetTop, "2")
				}
			});
	$("#canvas").mouseup(
			function(h) {
				c = false;
				kpl_context.clearRect(0, 0, 1800, 1200);
				var g = h.pageX - this.offsetLeft;
				var f = h.pageY - this.offsetTop;
				pressed = false;
				clickX.length = 0;
				clickY.length = 0;
				clickDrag.length = 0;
				console.log("mouseup!");
				pointEvent(h.pageX - this.offsetLeft, h.pageY - this.offsetTop,
						"1");
				var d = 0;
				if (record && !record_pause) {
					endTime = h.timeStamp;
					d = actionArray[actionArray.length - 1].actualaction;
					xbegin = actionArray[0].Xpoint * (1 / pRate);
					ybegin = actionArray[0].Ypoint * (1 / pRate);
					if (d == 0) {
						actionArray.push({
							Xpoint : g,
							Ypoint : f,
							actualaction : 0
						});
						if (endTime - beginTime >= 500) {
							//TODO delete #list click.
						} else {
							//TODO delete #list click.
						}
					} else {
						xend = actionArray[actionArray.length - 1].Xpoint
								* (1 / pRate);
						yend = actionArray[actionArray.length - 1].Ypoint
								* (1 / pRate);
						//TODO delete #list click.
					}
				}
				actionArray.length = 0
			});
	$("#canvas").mouseout(function(d) {
		if (pressed) {
			$("#canvas").trigger("mouseup")
		}
	});
	$("#canvas").mouseleave(function(d) {
		c = false;
		kpl_context.clearRect(0, 0, 1800, 1200)
	});
	function b(d) {
		if (record && !record_pause) {
			//TODO delete #list click.
		}
	}
	$("#home").mousedown(function(d) {
		beginTime = d.timeStamp;
		keyEvent(0, KEYCODE_HOME)
	});
	$("#home").mouseup(function(d) {
		endTime = d.timeStamp;
		keyEvent(1, KEYCODE_HOME);
		b("KEY_HOME")
	});
	$("#back").mousedown(function(d) {
		beginTime = d.timeStamp;
		keyEvent(0, KEYCODE_BACK)
	});
	$("#back").mouseup(function(d) {
		endTime = d.timeStamp;
		keyEvent(1, KEYCODE_BACK);
		b("KEY_BACK")
	});
	$("#menu").mousedown(function(d) {
		beginTime = d.timeStamp;
		keyEvent(0, KEYCODE_MENU)
	});
	$("#menu").mouseup(function(d) {
		endTime = d.timeStamp;
		keyEvent(1, KEYCODE_MENU);
		b("KEY_MENU")
	});
	$("#search").mousedown(function(d) {
		beginTime = d.timeStamp;
		keyEvent(0, KEYCODE_SEARCH)
	});
	$("#search").mouseup(function(d) {
		keyEvent(1, KEYCODE_SEARCH);
		endTime = d.timeStamp;
		b("KEY_SEARCH")
	});
	$("#wake").mousedown(function(d) {
		beginTime = d.timeStamp;
		keyEvent(0, 6)
	});
	$("#wake").mouseup(function(d) {
		endTime = d.timeStamp;
		keyEvent(1, 6);
		b("KEY_WAKE")
	});
	$("#rotate").click(function() {
		rotate()
	});
	$("#rotate").hover(function() {
		$("#rotate").popover("show")
	}, function() {
		$("#rotate").popover("hide")
	});
	$("#attach").click(function() {
		$("#fileupload").trigger("click")
	});
	$("#record").click(function() {
		if (!record) {
			$("#record").button("start");
			record = true;
			$("#pause").button("reset");
			beginTime = 0;
			endTime = 0
		} else {
			$("#record").button("reset");
			record = false;
			record_pause = false;
			var d = document.getElementById("pause");
			document.getElementById("pause").textContent = "暂停";
			document.getElementById("pause").disabled = "disabled";
			beginTime = 0;
			endTime = 0
		}
	});
	$("#pause").click(function() {
		if (!record_pause) {
			$("#pause").button("pause");
			record_pause = true;
			begintTime = 0;
			endTime = 0
		} else {
			$("#pause").button("reset");
			record_pause = false
		}
	});
	$("#play").click(function() {
		var g = $("#list").getDataIDs();
		if (g.length == 0) {
			alert("No Data in the table, cant replay!");
			return
		}
		this.disabled = true;
		var l = parseInt(prompt("回放次数", "1"));
		if (isNaN(l)) {
			alert("回放次数设置有误，请重新设置");
			this.disabled = false;
			return
		}
		mya = $("#list").getDataIDs();
		var h = $("#list").getRowData(mya[0]);
		var k = new Array();
		var f = 0;
		for ( var e in h) {
			k[f++] = e
		}
		var d = "";
		for (e = 0; e < mya.length; e++) {
			h = $("#list").getRowData(mya[e]);
			for (j = 0; j < k.length - 1; j++) {
				d = d + h[k[j]] + ","
			}
			d = d + ";"
		}
		replayOneEvent(d, l)
	});
	wakeScreen()
}
function wakeScreen() {
	keyEvent(1, KEYCODE_CLEAR)
}
function addClick(a, c, b) {
	clickX.push(a);
	clickY.push(c);
	clickDrag.push(b)
}
function keyEvent(c, a) {
	if (c == "0") {
		if (record && !record_pause) {
			if (beginTime != 0 && endTime != 0 && beginTime > endTime) {
				//TODO delete #list click.
			}
		}
	}
	var b = "key_" + c + "_" + a;
	sendEvent(b)
}
function pointEvent(x, y, flag) {
	console.log("x="+x+",y="+y+",flag="+flag+",pRate="+pRate);
	var ex = x;//x * (1 / pRate);//x * (1 / pRate);//
	var ey = y;//y * (1 / pRate);
	var b = ((rotateRate % 2) ? "h" : "v");
	var point = "pointer_" + flag + "_" + ex + "_" + ey + "_0_" + actionNumber + "_" + b;
	sendEvent(point)
}
function sendEvent(a) {
	if (2 == use_type) {
		return
	}
	if (wsSendEvent != null) {
		wsSendEvent.send(a)
	}
}
function updatecookies() {
	setCookie("lowres", document.getElementById("lowres").checked ? "true"
			: "false", 30)
}
function getCookie(a) {
	if (document.cookie.length > 0) {
		c_start = indof(a + "=", document.cookie);
		if (c_start != -1) {
			c_start = c_start + a.length + 1;
			c_end = indoff(";", document.cookie, c_start);
			if (c_end == -1) {
				c_end = document.cookie.length
			}
			return unescape(document.cookie.substring(c_start, c_end))
		}
	}
	return ""
}
function indof(b, a) {
	if (a.indexOf) {
		return a.indexOf(b)
	}
	for (var c = 0; c < a.length; c++) {
		if (a[c] == b) {
			return c
		}
	}
	return -1
}
function indoff(b, a, d) {
	if (a.indexOf) {
		return a.indexOf(b, d)
	}
	for (var c = d; c < a.length; c++) {
		if (a[c] == b) {
			return c
		}
	}
	return -1
}
function setCookie(b, c, a) {
	var d = new Date();
	d.setDate(d.getDate() + a);
	document.cookie = b + "=" + escape(c)
			+ ((a == null) ? "" : ";expires=" + d.toUTCString())
}
function releaseDevice() {
	var a = false;
	if ($("#rd_flag").length > 0) {
		if (confirm("调试进行中，确定离开吗？")) {
			a = true
		}
	} else {
		if (confirm("确定要离开吗?")) {
			a = true
		}
	}
	if (a == true) {
		var b = navigator.userAgent.toLowerCase();
		if (b.indexOf("qqbrowser") != -1) {
			$.ajax({
				url : ctx + "remoteAjax/lazyReleaseControl?id=" + usageId,
				async : false
			})
		}
		window.opener = null;
		window.open("", "_self");
		window.close()
	}
}
$("#script").click(function() {
	//del;
	/*if (confirm("上传脚本将会清空已录制的脚本")) {
		jQuery("#list").jqGrid("clearGridData");
		$("#scriptupload").trigger("click")
	}*/
});
function countDownTimer() {
	secToWait = secToWait - 1;
	if (secToWait <= 0) {
		secToWait = 59;
		minToWait = minToWait - 1
	}
	if (minToWait < 0) {
		clearInterval(/aa/);
		//alert("Sorry,剩余时间不足！");
		window.opener = null;
		window.open("", "_self");
		window.close();
		return
	}
	$("#counter").html(minToWait + "分" + secToWait + "秒");
	if (minToWait == 0 && secToWait == 59) {
		alert("您还剩1分钟使用时间，可以续期延长使用时间！")
	}
	if (minToWait == 4 && secToWait == 59) {
		$("#counter").css("color", "red")
	}
}
function renewalTime() {
	if (minToWait > 5) {
		alert("剩余5分钟以内允许续期。");
		return
	}
	$.ajax({
		url : myctx + "remoteAjax/ajaxRenewalTime?usageId=" + usageId,
		success : function(a) {
			if (a.rc == 0) {
				alert("data:"+a.msg);
				minToWait = a.remainMin;
				secToWait = a.remainSec;
				$("#counter").css("color", "#666667");
				$("#remoteAvailableNum").html(a.remoteAvailableNum)
			} else {
				alert("data:"+a.msg)
			}
		}
	})
}
function checkRemoteDebug(rd_token) {
	$.ajax({
		url : ctx + "remoteAjax/checkRemoteDebug?token=" + rd_token,
		success : function(data) {
			var obj = eval("(" + data.strRsp + ")");
			if (obj.rc != 0) {
				console.log("check remote debug error")
			} else {
				if (obj.remain_time < 180) {
					console.log("less 3 min");
					$("#rd_msg").html("调试时间不足3分钟，请尽快续期！")
				}
			}
		}
	})
}
function initSaveFile() {
	var b = document.getElementById("cutscreen");
	b.addEventListener("click", a, false);
	function a(i) {
		var f = "png";
		var h = deviceCanvas.toDataURL(f);
		var d = function(e) {
			e = e.toLowerCase().replace(/jpg/i, "jpeg");
			var k = e.match(/png|jpeg|bmp|gif/)[0];
			return "image/" + k
		};
		h = h.replace(d(f), "image/octet-stream");
		var g = function(m, e) {
			var k = document.createElementNS("http://www.w3.org/1999/xhtml",
					"a");
			k.href = m;
			k.download = e;
			var l = document.createEvent("MouseEvents");
			l.initMouseEvent("click", true, false, window, 0, 0, 0, 0, 0,
					false, false, false, false, 0, null);
			k.dispatchEvent(l)
		};
		var c = "kpl_" + (new Date()).getTime() + "." + f;
		g(h, c)
	}
}
function checkFileName(c) {
	var b = c.split("\\")[c.split("\\").length - 1];
	var a = /[\u0391-\uFFE5]+/g;
	if (b.match(a) != null) {
		alert("文件名不能包含汉字！");
		return true
	} else {
		return false
	}
}
function fileMaxLimited(b) {
	var a = b.files[0];
	if (a.size > 250 * 1000 * 1000) {
		alert("请上传小于" + 250 + "M的文件!");
		return true
	}
	return false
}
String.prototype.endWith = function(b) {
	var a = new RegExp(b + "$");
	return a.test(this)
};
$("#attach").tooltip("toggle");


function autoScale() {
	var a = $("div#device-wraper canvas").attr("width");
	if (a) {
		$("div#device-wraper").css("width", a);
		a = parseInt(a.replace("px", ""));
		if (a > 360) {
			var f = $("div#control-left").css("width");
			if (f) {
				var g = a - parseInt(f.replace("px", ""));
				$("div#control-left").css("width", a)
			}
			var d = $("div#control-right").css("width");
			if (d) {
				$("div#control-right").css("width",
						parseInt(d.replace("px", "")) - g)
			}
		}
	}
	var b = $("div#device-wraper canvas").attr("height");
	var e = 640;
	var c = 700;
	if (b) {
		e = parseInt(b.replace("px", "")) - 40;
		c = parseInt(b.replace("px", "")) - 70
	}
}
function getTop(a) {
	var b = a.offsetTop;
	if (a.offsetParent != null) {
		b += getTop(a.offsetParent)
	}
	return b
}
function getLeft(a) {
	var b = a.offsetLeft;
	if (a.offsetParent != null) {
		b += getLeft(a.offsetParent)
	}
	return b
}
function showBg() {
	/*var a = document.getElementById("loadbar");
	if (a != null) {
		a.style.display = ""
	}*/
}
function closeBg() {
	/*var a = document.getElementById("loadbar");
	if (a != null) {
		a.style.display = "none"
	}*/
}
function ajaxInitDevice() {
	$.ajax({
		url :myctx + "/remoteAjax/ajaxInitDevice?deviceId=" + id + "&usageId="
				+ usageId,
		success : function(a) {
			if (a.rc == 0) {
				initFlag = true;
				if (imgIfValidFlag && !loadingIfCloseFlag) {
					loadingIfCloseFlag = true;
					closeBg()
				}
			} else {
				alert("init data:"+a.msg);
				window.close()
			}
		}
	})
}
var flag = 0;
/*$("#saveSuggest").click(
		function() {
			var a = "";
			$("input[name='suggestType']:checked").each(function() {
				a = a + ";" + $(this).val()
			});
			if (a == "") {
				alert("请选择反馈问题类型！");
				return false
			}
			var b = $("#remark").val();
			if (flag == 0) {
				flag = 1;
				$.ajax({
					url : ctx + "remoteAjax/ajaxSaveSuggest?deviceId=" + id
							+ "&suggestType=" + a + "&remark=" + b,
					success : function(c) {
						alert(c.msg);
						flag = 0;
						//$("#suggestDiv").CloseDiv()
					}
				})
			}
		});*/
$("#rotate-screen").click(function() {
	/*$("#remark").val("");
	$("input[name='suggestType']:checked").attr("checked", false);
	$("#suggestDiv").OpenDiv()*/
});
window.onresize = function() {
	/*if ($("#suggestDiv").css("display") != "none") {
		$("#suggestDiv").CloseDiv();//del
		$("#suggestDiv").OpenDiv()//del;
	}*/
};
$("#suggestClose").click(function() {
	//$("#suggestDiv").CloseDiv()
});

