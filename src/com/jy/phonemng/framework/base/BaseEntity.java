package com.jy.phonemng.framework.base;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.beanutils.BeanUtils;

/**
 * <p> 抽象实体基类，提供统一的ID，和相关的基本功能方法
 * 如果是oracle请参考{@link BaseOracleEntity}
 * 
 * @author kejun.song
 * @version $Id: BaseEntity.java, v 0.1
 */
@MappedSuperclass
public abstract class BaseEntity<ID extends Serializable> extends AbstractEntity<ID> {

    /**  */
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private ID                id;

    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date            created_date;

    @Column(name = "update_date")
    @Temporal(TemporalType.TIMESTAMP)
    protected Date            update_date;

    public Date getCreated_date() {
        return created_date;
    }

    public void setCreated_date(Date created_date) {
        this.created_date = created_date;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    /**
     * Setter method for property <tt>update_at</tt>.
     * 
     * @param update_at value to be assigned to property update_at
     */
    public void setUpdate_at(Date update_date) {
        this.update_date = update_date;
    }

    @Override
    public ID getId() {
        return id;
    }

    @Override
    public void setId(ID id) {
        this.id = id;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public String toString() {
        try {
            Map props = BeanUtils.describe(this);
            Set iProps = props.entrySet();
            StringBuffer retBuffer = new StringBuffer();
            for (Iterator iter = iProps.iterator(); iter.hasNext();) {
                Entry entry = (Entry) iter.next();
                String key = (String) entry.getKey();
                Object value = entry.getValue();
                // skip false property "class"
                if ("class".equals(key)) {
                    continue;
                }

                retBuffer.append(key).append("=[").append(value).append("]");

                if (iter.hasNext()) {
                    retBuffer.append(", ");
                }
            }
            return retBuffer.toString();
        } catch (IllegalAccessException e1) {
            return super.toString();
        } catch (InvocationTargetException e1) {
            return super.toString();
        } catch (NoSuchMethodException e1) {
            return super.toString();
        }
    }
}
