package com.jy.phonemng.framework.annotation;

public enum UserStatus {

    normal("����״̬"), blocked("���״̬");

    private final String info;

    private UserStatus(String info) {
        this.info = info;
    }

    public String getInfo() {
        return info;
    }
}
