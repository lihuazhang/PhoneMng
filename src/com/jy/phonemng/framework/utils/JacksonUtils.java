/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package com.jy.phonemng.framework.utils;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.ObjectMapper;

import com.alibaba.common.lang.StringUtil;
import com.jy.phonemng.biz.device.pojo.Command;

/**
 * 
 * @author kejun.song
 * @version $Id: JacksonUtil.java, v 0.1 2015��8��27�� ����7:18:40 kejun.song Exp $
 */
public class JacksonUtils {

    private static ObjectMapper mapper = new ObjectMapper(); ;

    public static <T> T toBean(String jsonStr, Class<T> valueType) {
        if (mapper == null) {
            mapper = new ObjectMapper();
        }

        try {
            return mapper.readValue(jsonStr, valueType);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String beanToJson(Object obj) {
        if (obj == null) {
            return null;
        }
        String json = null;
        try {
            StringWriter writer = new StringWriter();
            JsonGenerator gen = new JsonFactory().createJsonGenerator(writer);
            mapper.writeValue(gen, obj);
            gen.close();
            json = writer.toString();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return json;
    }

    public static void main(String[] args) {

        Command cmd = new Command();

        cmd.setCommand("click");

        List<HashMap<String, Object>> params = new ArrayList<HashMap<String, Object>>();
        HashMap<String, Object> map = new HashMap<String, Object>();

        map.put("x", Integer.valueOf("122"));
        map.put("y", (int) Double.parseDouble("220.6166"));

        params.add(map);
        cmd.setParams(params);

        try {
            System.out.println(JacksonUtils.beanToJson(cmd));
            String ipsString = "IP/127.0.0.1:64735";
            System.out.println(StringUtil.substringBetween(ipsString, "IP/", ":"));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
