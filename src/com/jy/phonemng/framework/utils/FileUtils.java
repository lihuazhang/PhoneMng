/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package com.jy.phonemng.framework.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * 
 * @author kejun.song
 * @version $Id: FileUtils.java, v 0.1 2015年8月19日 下午7:42:30 kejun.song Exp $
 */
public class FileUtils {

    public static byte[] readFileToByte(String filename) throws IOException {

        File file = new File(filename);
        if (filename == null || filename.equals("")) {
            throw new NullPointerException("无效的文件路径");
        }
        long len = file.length();
        byte[] bytes = new byte[(int) len];
        BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
        @SuppressWarnings("unused")
        int r = bufferedInputStream.read(bytes);

        bufferedInputStream.close();
        return bytes;

    }

    /*public static String byte2hex(byte[] b) {
    StringBuffer sb = new StringBuffer();
    String stmp = "";
    for (int n = 0; n < b.length; n++) {
        stmp = Integer.toHexString(b[n] & 0XFF);
        if (stmp.length() == 1) {
            sb.append("0" + stmp);
        } else {
            sb.append(stmp);
        }

    }
    return sb.toString();
    }*/

    ///
    /* File f = new File("D:\\JavaScript\\leftphone.png");
     FileInputStream fis = new FileInputStream(f);
     byte[] bytes = new byte[fis.available()];
     fis.read(bytes);
     fis.close();*/

    // 生成字符串
    // String imgStr = byte2hex(bytes);
    // System.out.println("########imgStr:" + imgStr);
    // TextMessage txtMessage = new TextMessage(d);
}
