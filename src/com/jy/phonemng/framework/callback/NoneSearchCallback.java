package com.jy.phonemng.framework.callback;

import javax.persistence.Query;

import com.jy.phonemng.framework.search.Searchable;

/****
 * 
 * 
 * @author kejun.song
 * @version $Id: NoneSearchCallback.java, v 0.1 2014��11��19�� ����10:58:17 kejun.song Exp $
 */
public final class NoneSearchCallback implements SearchCallback {

    @Override
    public void prepareQL(StringBuilder ql, Searchable search) {
    }

    @Override
    public void prepareOrder(StringBuilder ql, Searchable search) {
    }

    @Override
    public void setValues(Query query, Searchable search) {
    }

    @Override
    public void setPageable(Query query, Searchable search) {
    }
}
