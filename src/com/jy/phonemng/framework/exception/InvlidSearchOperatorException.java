package com.jy.phonemng.framework.exception;

import com.jy.phonemng.framework.search.SearchOperator;

/***
 * 
 * 
 * @author kejun.song
 * @version $Id: InvlidSearchOperatorException.java, v 0.1 2014��11��19�� ����10:25:48 kejun.song Exp $
 */
public final class InvlidSearchOperatorException extends SearchException {

    /**  */
    private static final long serialVersionUID = 1L;

    public InvlidSearchOperatorException(String searchProperty, String operatorStr) {
        this(searchProperty, operatorStr, null);
    }

    public InvlidSearchOperatorException(String searchProperty, String operatorStr, Throwable cause) {
        super("Invalid Search Operator searchProperty [" + searchProperty + "], " + "operator ["
              + operatorStr + "], must be one of " + SearchOperator.toStringAllOperator(), cause);
    }
}
