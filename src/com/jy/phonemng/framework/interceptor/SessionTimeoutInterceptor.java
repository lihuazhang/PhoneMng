/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package com.jy.phonemng.framework.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.jy.phonemng.framework.utils.Constants;

/**
 * 
 * @author kejun.song
 * @version $Id: SessionTimeoutInterceptor.java, v 0.1 2015年8月4日 上午10:34:57 kejun.song Exp $
 */
public class SessionTimeoutInterceptor implements HandlerInterceptor {

    protected Logger logger = LoggerFactory.getLogger(SessionTimeoutInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
                             Object handler) throws Exception {

        String requestUrl = request.getRequestURI();
        logger.info("请求URL=" + requestUrl);
        Object obj = request.getSession().getServletContext().getAttribute(Constants.CURRENT_USER);
        if (obj != null) {
            return true;
        } else {
            response.setHeader("sessionstatus", "timeout");
            return false;
        }

    }

    @Override
    public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2,
                           ModelAndView arg3) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2,
                                Exception arg3) throws Exception {
    }

}
