/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package com.jy.phonemng.biz.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.WebSocketConnectionManager;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.handler.AbstractWebSocketHandler;

import com.jy.phonemng.biz.dal.model.Device;
import com.jy.phonemng.biz.dal.model.TransResult;
import com.jy.phonemng.biz.dal.model.User;
import com.jy.phonemng.biz.device.service.DeviceService;
import com.jy.phonemng.biz.utils.StatusEnum;
import com.jy.phonemng.framework.base.BaseCRUDController;
import com.jy.phonemng.framework.search.Searchable;
import com.jy.phonemng.framework.utils.Constants;
import com.jy.phonemng.framework.utils.JacksonUtils;

/**
 * 
 * @author kejun.song
 * @version $Id: DeviceController.java, v 0.1 2015年8月11日 下午3:06:00 kejun.song Exp $
 */
@Controller
@RequestMapping(value = "/biz/device")
public class DeviceController extends BaseCRUDController<Device, Long> {

    protected Logger                                 logger    = LoggerFactory
                                                                   .getLogger(DeviceController.class);

    private static ConcurrentHashMap<Object, Device> deviceMap = new ConcurrentHashMap<Object, Device>();

    @Autowired
    protected DeviceService                          deviceService;

    @RequestMapping(value = "/sigleDevcePage.htm", method = RequestMethod.GET)
    public String sigleDevcePage(HttpServletRequest request, HttpServletResponse response,
                                 Model model) {

        User loginUser = (User) request.getSession().getAttribute(Constants.CURRENT_USER);
        if (loginUser == null) {
            return "redirect:/biz/user/loginview";
        }

        String deviceId = request.getParameter("id");
        logger.info("#debug获取设备ID:" + deviceId);

        if (loginUser != null) {
            Device device = deviceMap.get(deviceId);
            if (device == null) {
                device = deviceService.findOne(Long.valueOf(deviceId));
                deviceMap.put(device, device);
                if (device.getUserId() == null) {
                    device.setUserId(loginUser.getId());
                    deviceService.update(device);
                }
            }
            request.setAttribute("id", device.getId());
            request.getSession().setAttribute("user-deviceid", loginUser.getId() + "_" + deviceId);
        }

        return "/biz/device/singleDevPage";
    }

    @RequestMapping(value = "/showDeviceList.htm", method = RequestMethod.GET)
    public String showDeviceList(HttpServletRequest request, HttpServletResponse response,
                                 Model model) {

        return "/biz/device/deviceList";
    }

    @RequestMapping(value = "/regester", method = RequestMethod.GET)
    public @ResponseBody String regesterDevice(HttpServletRequest request,
                                               HttpServletResponse response) {

        String devcieInfo = request.getParameter("deviceInfo");

        try {
            System.out.println("设备注册JSON:" + URLDecoder.decode(devcieInfo, "utf-8"));

            String jsonStr = URLDecoder.decode(devcieInfo, "utf-8");

            @SuppressWarnings("unchecked")
            List<Device> vo = JacksonUtils.toBean(jsonStr, List.class);
            if (vo != null) {
                for (Object o : vo) {
                    @SuppressWarnings("unchecked")
                    LinkedHashMap<String, Object> deviceMap = (LinkedHashMap<String, Object>) o;
                    Device device = convertToDevice(deviceMap);

                    //search 
                    Map<String, Object> searchParams = new HashMap<String, Object>();
                    searchParams.put("serialNo_eq", device.getSerialNo());

                    Searchable searchable = Searchable.newSearchable(searchParams);

                    List<Device> devFind = deviceService.findAllWithSort(searchable);

                    if (devFind != null && devFind.size() > 0) {
                        logger.info("设备已经存在:" + devFind);
                        Device de = devFind.get(0);
                        de.setAgentMac(device.getAgentMac());
                        de.setStatus(StatusEnum.ONLINE.getValue());
                        deviceService.update(de);
                    } else {
                        device.setStatus(StatusEnum.ONLINE.getValue());
                        Device devRes = deviceService.save(device);
                        logger.info("设备保持成功=" + devRes);
                    }
                }
            }

        } catch (UnsupportedEncodingException e) {
            logger.error("", e);
        }

        TransResult result = new TransResult();

        result.setResult("success");

        String strRes = JacksonUtils.beanToJson(result);
        System.out.println("设备注册返回结果:" + strRes);
        return strRes;
    }

    @RequestMapping(value = "/remoteAjax/ajaxInitDevice", method = RequestMethod.GET)
    public @ResponseBody ModelMap ajaxInit(HttpServletRequest request,
                                           HttpServletResponse response, String deviceId,
                                           String usageId) {

        ModelMap modelMap = new ModelMap();
        modelMap.addAttribute("rc", 0);
        modelMap.addAttribute("msg", "ajaxInit msg.");
        return modelMap;
    }

    @RequestMapping(value = "/http/getImage", method = RequestMethod.GET)
    public @ResponseBody ModelMap getImage(HttpServletRequest request,
                                           HttpServletResponse response, String id, String imgParam) {

        ModelMap modelMap = new ModelMap();
        logger.info("######################");
        String uri = "ws://localhost:8080/PhoneMng/biz/device/wsServlet/getImage";
        WebSocketClient client = new StandardWebSocketClient();
        AbstractWebSocketHandler handler = new AbstractWebSocketHandler() {
            @Override
            public void afterConnectionEstablished(WebSocketSession session) throws Exception {
                session.sendMessage(new TextMessage("####ddd---dd###"));
            }

            @Override
            public void handleTextMessage(WebSocketSession session, TextMessage message)
                                                                                        throws Exception {
                System.out.println("##Message## " + message.getPayload());
                session.sendMessage(new TextMessage("##hello##"));

            }
        };

        WebSocketConnectionManager manager = new WebSocketConnectionManager(client, handler, uri);
        manager.start();

        return modelMap;
    }

    //  @RequestMapping(value = "/wsServlet/getLogCat111", method = RequestMethod.GET)
    public @ResponseBody ModelMap getLogCat(HttpServletRequest request,
                                            HttpServletResponse response, String id,
                                            String totalTimeOut) {

        ModelMap modelMap = new ModelMap();
        return modelMap;
    }

    // @RequestMapping(value = "/wsServlet/sendEvent111", method = RequestMethod.GET)
    public @ResponseBody ModelMap sendEvent(HttpServletRequest request,
                                            HttpServletResponse response, String id,
                                            String totalTimeOut, String usageId) {

        ModelMap modelMap = new ModelMap();
        return modelMap;
    }

    private Device convertToDevice(LinkedHashMap<String, Object> map) {
        Device device = new Device();
        device.setAgentMac(map.get("agentMac").toString());
        device.setBrand(map.get("brand").toString());

        if (map.get("height") != null) {
            device.setHeight(Integer.parseInt(map.get("height").toString()));
        }
        device.setIconName(map.get("brand").toString());
        device.setLocation(map.get("location").toString());
        device.setMac(map.get("mac").toString());
        device.setManufacture(map.get("manufacture").toString());
        device.setOs(map.get("os").toString());
        device.setOsVersion(map.get("os_version").toString());
        device.setResolution(map.get("width").toString() + "*" + map.get("height"));
        device.setSerialNo(map.get("sn").toString());
        device.setSimCard(map.get("sim_card").toString());
        device.setStatus(map.get("status").toString());
        if (map.get("userId") != null && !"".equalsIgnoreCase(map.get("userId").toString())) {
            device.setUserId(Long.parseLong(map.get("userId").toString()));
        }
        if (map.get("width") != null) {
            device.setWidth(Integer.parseInt(map.get("width").toString()));
        }
        device.setCreated_date(new Date());
        device.setUpdate_at(new Date());
        return device;
    }
}
