/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package com.jy.phonemng.biz.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jy.phonemng.biz.dal.model.Log;
import com.jy.phonemng.biz.log.service.LogService;
import com.jy.phonemng.framework.base.BaseCRUDController;

/**
 * 
 * @author kejun.song
 * @version $Id: LogController.java, v 0.1 2015��8��5�� ����7:57:49 kejun.song Exp $
 */
@Controller
@RequestMapping(value = "/biz/log")
public class LogController extends BaseCRUDController<Log, Long> {

    @Autowired
    protected LogService logService;

}
