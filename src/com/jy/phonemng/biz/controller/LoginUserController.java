package com.jy.phonemng.biz.controller;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jy.phonemng.biz.dal.model.TransResult;
import com.jy.phonemng.biz.dal.model.User;
import com.jy.phonemng.biz.user.service.UserService;
import com.jy.phonemng.biz.user.service.UserStatusHistoryService;
import com.jy.phonemng.biz.utils.UserUtils;
import com.jy.phonemng.framework.base.BaseController;
import com.jy.phonemng.framework.utils.Constants;
import com.jy.phonemng.framework.utils.JacksonUtils;

@Controller()
public class LoginUserController extends BaseController<User, Long> {

    @Value(value = "${shiro.login.url}")
    private String                   loginUrl;

    @Autowired
    private MessageSource            messageSource;
    @Autowired
    protected UserService            userService;

    @Autowired
    private CacheManager             ehcacheManager;

    @Autowired
    private UserStatusHistoryService userStatusHistoryService;

    private Cache                    loginUserCache;

    @PostConstruct
    public void init() {
        loginUserCache = ehcacheManager.getCache("sys-userCache");
    }

    @RequestMapping(value = { "/{login:login;?.*}" })
    public String loginForm(HttpServletRequest request, ModelMap model, User user) {
        logger.info("LoginForm starting...");
        logger.info("##user==" + user);
        //表示退出
        if (!StringUtils.isEmpty(request.getParameter("logout"))) {
            model.addAttribute(Constants.MESSAGE,
                messageSource.getMessage("user.logout.success", null, null));
        }

        //表示用户删除了 @see org.apache.shiro.web.filter.user.SysUserFilter
        if (!StringUtils.isEmpty(request.getParameter("notfound"))) {
            model.addAttribute(Constants.ERROR,
                messageSource.getMessage("user.notfound", null, null));
        }

        //表示用户被管理员强制退出
        if (!StringUtils.isEmpty(request.getParameter("forcelogout"))) {
            model.addAttribute(Constants.ERROR,
                messageSource.getMessage("user.forcelogout", null, null));
        }

        //表示用户输入的验证码错误
        if (!StringUtils.isEmpty(request.getParameter("jcaptchaError"))) {
            model.addAttribute(Constants.ERROR,
                messageSource.getMessage("jcaptcha.validate.error", null, null));
        }

        //表示用户锁定了 @see org.apache.shiro.web.filter.user.SysUserFilter
        if (!StringUtils.isEmpty(request.getParameter("blocked"))) {
            User user2 = (User) request.getAttribute(Constants.CURRENT_USER);
            String reason = userStatusHistoryService.getLastReason(user2);
            model.addAttribute(Constants.ERROR,
                messageSource.getMessage("user.blocked", new Object[] { reason }, null));
        }

        if (!StringUtils.isEmpty(request.getParameter("unknown"))) {
            model.addAttribute(Constants.ERROR,
                messageSource.getMessage("user.unknown.error", null, null));
        }

        //登录失败了 提取错误消息
        Exception shiroLoginFailureEx = (Exception) request
            .getAttribute(FormAuthenticationFilter.DEFAULT_ERROR_KEY_ATTRIBUTE_NAME);
        if (shiroLoginFailureEx != null) {
            model.addAttribute(Constants.ERROR, shiroLoginFailureEx.getMessage());
        }

        //如果用户直接到登录页面 先退出一下
        //原因：isAccessAllowed实现是subject.isAuthenticated()---->即如果用户验证通过 就允许访问
        // 这样会导致登录一直死循环
        Subject subject = SecurityUtils.getSubject();
        if (subject != null && subject.isAuthenticated()) {
            subject.logout();
        }

        //如果同时存在错误消息 和 普通消息  只保留错误消息
        if (model.containsAttribute(Constants.ERROR)) {
            model.remove(Constants.MESSAGE);
        }

        return "redirect:/login";
    }

    /**
     * 管理登录
     * @throws Exception 
     */
    @RequestMapping(value = "/login")
    public String login(HttpServletRequest request, HttpServletResponse response, User user,
                        Model model) throws Exception {

        User u = UserUtils.getUser();
        logger.info("get user from catch=" + u);
        logger.info("=======================================\n");
        // 如果已经登录，则跳转到管理首页
        if (user != null) {
            logger.info("##### get user=" + user.toString());
            String passwd = user.getPassword();
            user = userService.findByEmail(user.getEmail());
            if (user != null) {
                user = userService.login(user.getEmail(), passwd);
            } else {
                model.addAttribute("error", "用户不存在");
                throw new Exception("用户不存在");
            }
            // put cache;
            loginUserCache.put(new Element(user.getUserName(), user));
            //put login session
            request.getSession().setAttribute(Constants.CURRENT_USER, user);
            model.addAttribute("page", null);

            return "redirect:/";
        }

        return "redirect:/";
    }

    @RequestMapping(value = "/biz/user/registerUser", method = RequestMethod.GET)
    public String register(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        logger.info("User register starting...");

        return "biz/user/registerUser";
    }

    @RequestMapping(value = "/biz/user/loginview", method = RequestMethod.GET)
    public String loginTo(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        logger.info("User register starting...");

        return "biz/user/login";
    }

    @RequestMapping(value = "/biz/user/submitRegUser.htm", method = RequestMethod.POST)
    public String submitRegister(HttpServletRequest request, HttpServletResponse response,
                                 ModelMap model, User user) {
        logger.info("submitRegUser register starting...");
        logger.info("用户注册");
        if (user != null) {
            logger.info("Register User =" + user);
        }

        return "redirect:/";
    }

    @RequestMapping(value = "/biz/user/agentUserLogin.htm", method = RequestMethod.GET)
    public @ResponseBody String agentUserLogin(HttpServletRequest request,
                                               HttpServletResponse response) {

        String userid = request.getParameter("userid");
        String encripPass = request.getParameter("encpass");

        System.out.println("agent user login id=" + userid);
        System.out.println("agent user login pass=" + encripPass);

        User lUser = userService.login("hezheng", "123456");
        TransResult result = new TransResult();
        if (lUser != null) {
            request.getSession().setAttribute(lUser.getUserName(), lUser);

            result.setResult("success");
            result.setStatus("login");
            result.setToken(lUser.getSalt());
            result.setUserId(lUser.getUserName());
            System.out.println("从session中获取user:" + request.getSession().getAttribute(userid));

            return JacksonUtils.beanToJson(result);
        } else {
            result.setResult("fail");
            return JacksonUtils.beanToJson(result);
        }

    }

}
