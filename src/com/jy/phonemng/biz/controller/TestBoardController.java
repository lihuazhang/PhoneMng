/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package com.jy.phonemng.biz.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 
 * @author kejun.song
 * @version $Id: TestBoardController.java, v 0.1 2015��8��11�� ����2:22:22 kejun.song Exp $
 */
@Controller
public class TestBoardController {

    protected Logger logger = LoggerFactory.getLogger(TestBoardController.class);

    @RequestMapping(value = "/biz/test/testMainPage.htm", method = RequestMethod.GET)
    public String testMainPage(HttpServletRequest request, HttpServletResponse response, Model model) {

        return "/biz/test/testMain";
    }
}
