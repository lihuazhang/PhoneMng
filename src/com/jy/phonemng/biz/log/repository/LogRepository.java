/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package com.jy.phonemng.biz.log.repository;

import com.jy.phonemng.biz.dal.model.Log;
import com.jy.phonemng.framework.base.BaseRepository;

/**
 * 
 * @author kejun.song
 * @version $Id: LogRepository.java, v 0.1 2015��8��5�� ����7:51:57 kejun.song Exp $
 */
public interface LogRepository extends BaseRepository<Log, Long> {

}
