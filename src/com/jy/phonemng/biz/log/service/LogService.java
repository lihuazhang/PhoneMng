/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package com.jy.phonemng.biz.log.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.jy.phonemng.biz.dal.model.Log;
import com.jy.phonemng.biz.log.repository.LogRepository;
import com.jy.phonemng.framework.base.BaseService;

/**
 * 
 * @author kejun.song
 * @version $Id: LogService.java, v 0.1 2015��8��5�� ����7:53:49 kejun.song Exp $
 */
@Service
public class LogService extends BaseService<Log, Long> {

    private LogRepository getLogRepository() {
        return (LogRepository) baseRepository;
    }

    public Page<Log> findByPage(Log log, Pageable pageable) {
        return getLogRepository().findAll(pageable);
    }

}
