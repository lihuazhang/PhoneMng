/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package com.jy.phonemng.biz.minaserver;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.charset.Charset;
import java.util.concurrent.Executors;

import org.apache.mina.core.filterchain.DefaultIoFilterChainBuilder;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.textline.LineDelimiter;
import org.apache.mina.filter.codec.textline.TextLineCodecFactory;
import org.apache.mina.filter.executor.ExecutorFilter;
import org.apache.mina.filter.logging.LoggingFilter;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author kejun.song
 * @version $Id: CommandServer.java, v 0.1 2015��9��2�� ����6:52:01 kejun.song Exp $
 */
public class CommandServer {
    protected static Logger  logger      = LoggerFactory.getLogger(CommandServer.class);
    public static final int  PORT        = 33788;
    private final static int maxNioCache = 10 * 1024 * 1024;

    public static void main(String[] args) {

        CommandServerIoHandler handler = new CommandServerIoHandler();

        NioSocketAcceptor acceptor = new NioSocketAcceptor();

        acceptor.getSessionConfig().setReadBufferSize(maxNioCache);
        acceptor.getSessionConfig().setReceiveBufferSize(maxNioCache);
        acceptor.getSessionConfig().setSendBufferSize(maxNioCache);

        acceptor.getFilterChain().addLast("logger", new LoggingFilter());

        TextLineCodecFactory factory = new TextLineCodecFactory(Charset.forName("UTF-8"),
            LineDelimiter.WINDOWS.getValue(), LineDelimiter.WINDOWS.getValue());

        acceptor.getFilterChain().addLast("protocol", new ProtocolCodecFilter(factory));

        DefaultIoFilterChainBuilder filterChainBuilder = acceptor.getFilterChain();
        filterChainBuilder.addLast("threadPool",
            new ExecutorFilter(Executors.newCachedThreadPool()));

        acceptor.setHandler(handler);
        try {
            acceptor.bind(new InetSocketAddress(PORT));
            System.out.println("Command server opening...");
        } catch (IOException e) {
            logger.error("bind error:", e);
        }
    }
}
