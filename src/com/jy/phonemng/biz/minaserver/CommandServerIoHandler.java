package com.jy.phonemng.biz.minaserver;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.common.lang.StringUtil;
import com.jy.phonemng.biz.dal.model.Device;
import com.jy.phonemng.biz.device.pojo.Command;
import com.jy.phonemng.biz.device.service.DeviceService;
import com.jy.phonemng.biz.utils.StatusEnum;
import com.jy.phonemng.framework.search.Searchable;
import com.jy.phonemng.framework.utils.JacksonUtils;
import com.jy.phonemng.framework.utils.SpringUtils;

/**
 * 
 * @author kejun.song
 * @version $Id: CommandServerIoHandler.java, v 0.1 2015年9月2日 下午6:52:55 kejun.song Exp $
 */
public class CommandServerIoHandler extends IoHandlerAdapter {

    private static ConcurrentHashMap<Object, IoSession> ioSessionMap  = new ConcurrentHashMap<Object, IoSession>();

    private static Logger                               logger        = LoggerFactory
                                                                          .getLogger(CommandServerIoHandler.class);
    protected DeviceService                             deviceService = SpringUtils
                                                                          .getBean(DeviceService.class);

    private static ConcurrentHashMap<Object, Device>    deviceMap     = new ConcurrentHashMap<Object, Device>();

    @Override
    public void sessionCreated(IoSession session) {
        logger.info("Command 服务端与客户端创建连接");

    }

    @Override
    public void sessionOpened(IoSession session) throws Exception {
        logger.info("Command session opened:" + session);
    }

    @Override
    public void exceptionCaught(IoSession session, Throwable cause) throws Exception {
        logger.warn(cause.getMessage(), cause);
    }

    /***
     * 1.Agent注册设备后；存储Mina session，key=user-deviceid; <br>
     * 2. command 发送，根据自己的userid-deviceid 获取session决定发送给哪个session。
     * @see org.apache.mina.core.service.IoHandlerAdapter#messageReceived(org.apache.mina.core.session.IoSession, java.lang.Object)
     */
    @Override
    public void messageReceived(IoSession session, Object message) throws Exception {
        logger.info("Command服务端接收:cmd=" + message + ";session=" + session);

        Command command = JacksonUtils.toBean(message.toString(), Command.class);
        if (command == null) {
            logger.info("接受到command对象为空.");
            return;
        }
        logger.info("Command服务端接收对象:" + command);
        String pcMac = null;
        String from = null;

        if (command.getParams() != null) {
            from = command.getParams().get(0).get("from").toString();
            if ("agent".equalsIgnoreCase(from)) {
                pcMac = command.getParams().get(0).get("pc_mac").toString();
                logger.info("command用户Session标识:" + pcMac);
                IoSession tempIoSession = ioSessionMap.get(pcMac);
                if (tempIoSession == null) {
                    ioSessionMap.put(pcMac, session);
                    logger.info("Command保存session完毕。pcMac=" + pcMac);
                    session.setAttributeIfAbsent("mac", pcMac);
                    logger.info("目前command链接session个数:" + ioSessionMap.values().size());
                } else {
                    tempIoSession.write(message);
                    logger.info("发送到command客户端:=" + message + ",tempIoSession:" + tempIoSession);
                }
            } else {
                //查询DB get pc mac；
                String userdevId = command.getParams().get(0).get("user-deviceid").toString();
                if (userdevId != null) {
                    String[] uid = userdevId.split("_");
                    String deviceId = uid[1];
                    logger.info("获取设备ID：id=" + deviceId);
                    Device device = deviceMap.get(deviceId);
                    if (device == null) {
                        device = deviceService.findOne(Long.valueOf(deviceId));
                        deviceMap.put(deviceId, device);
                        logger.info("设备更新缓存成功。");
                    }

                    String mac = null;
                    if (device != null) {
                        mac = device.getAgentMac();
                        logger.info("查询到Mac=" + mac);
                    }
                    IoSession tempIoSession = ioSessionMap.get(mac);
                    if (tempIoSession != null) {
                        tempIoSession.write(message);
                        logger.info("Session map获取不为空,send=" + message);
                    } else {
                        logger.info("没有获取到Mac的session，mac=" + mac);
                    }
                }
                logger.info("session来自adapter,message=" + message);
            }

        } else {
            logger.info("Command中参数为空NULL,不处理。");
            return;
        }

    }

    @Override
    public void sessionIdle(IoSession session, IdleStatus status) throws Exception {
        logger.info("Command session IDLE " + session.getIdleCount(status));
    }

    @Override
    public void sessionClosed(IoSession session) throws Exception {
        logger.info("==command Session closed.==" + session.getId());
        String mac = (String) session.getAttribute("mac");
        ioSessionMap.remove(mac);
        logger
            .info("Commandsession关闭" + mac + ",ioSessionMap size:" + ioSessionMap.values().size());
        Map<String, Object> searchParams = new HashMap<String, Object>();
        searchParams.put("agentMac_eq", mac);
        Searchable searchable = Searchable.newSearchable(searchParams);
        List<Device> devFind = deviceService.findAllWithSort(searchable);
        if (devFind != null && devFind.size() > 0) {
            for (Device dev : devFind) {
                Device tempDevice = deviceService.findOne(dev.getId());
                if (tempDevice != null) {
                    tempDevice.setStatus(StatusEnum.OFFLINE.getValue());
                    deviceService.update(tempDevice);
                    logger.info("session closed and set device to offline.Device id="
                                + tempDevice.getId());
                }
            }
        }

    }

    @Override
    public void messageSent(IoSession session, Object message) throws Exception {
        logger.info("Command服务端向客户端[" + session.getAttribute("mac") + "]发送" + message + "信息成功");
    }

    public String getIP(String remoteAdd) {
        if (remoteAdd == null) {
            return null;
        }
        if (remoteAdd.indexOf("/") < 0) {
            return null;
        }
        return StringUtil.substringBetween(remoteAdd, "/", ":");
    }
}
