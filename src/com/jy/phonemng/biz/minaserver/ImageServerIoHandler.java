package com.jy.phonemng.biz.minaserver;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jy.phonemng.biz.dal.model.Device;
import com.jy.phonemng.biz.device.pojo.Command;
import com.jy.phonemng.biz.device.service.DeviceService;
import com.jy.phonemng.biz.minacodec.ImageRequest;
import com.jy.phonemng.framework.search.Searchable;
import com.jy.phonemng.framework.utils.JacksonUtils;
import com.jy.phonemng.framework.utils.SpringUtils;

public class ImageServerIoHandler extends IoHandlerAdapter {

    //private final static String                         characters   = "mina rocks abcdefghijklmnopqrstuvwxyz0123456789";

    public static final String                          INDEX_KEY     = ImageServerIoHandler.class
                                                                          .getName() + ".INDEX";

    private static Logger                               logger        = LoggerFactory
                                                                          .getLogger(ImageServerIoHandler.class);

    private static ConcurrentHashMap<Object, IoSession> ioSessionMap  = new ConcurrentHashMap<Object, IoSession>();

    protected DeviceService                             deviceService = SpringUtils
                                                                          .getBean(DeviceService.class);

    private static ConcurrentHashMap<Object, Device>    deviceMap     = new ConcurrentHashMap<Object, Device>();

    @Override
    public void sessionCreated(IoSession session) throws Exception {
        logger.info("服务端与客户端创建连接session:" + session);
    }

    @Override
    public void sessionOpened(IoSession session) throws Exception {
        logger.info("Image server session opened:" + session);
    }

    @Override
    public void exceptionCaught(IoSession session, Throwable cause) throws Exception {
        logger.warn(cause.getMessage(), cause);
    }

    /**
     * Handle incoming messages.
     * 
     * 
     */
    @Override
    public void messageReceived(IoSession session, Object message) throws Exception {
        logger.info("Image服务端接收:message=" + message + ";session=" + session);

        ImageRequest request = (ImageRequest) message;
        logger.info("==>Image session来自:" + request.getFrom());
        Command command = JacksonUtils.toBean(request.getData(), Command.class);
        logger.info("Image服务端接收对象:" + command);

        String from = null;
        byte[] reqImage = request.getImageContents();//

        if (command.getParams() != null) {
            from = command.getParams().get(0).get("from").toString();
            //pcMac = command.getParams().get(0).get("pc_mac").toString();
            if ("agent".equalsIgnoreCase(from)) {
                //search 
                Map<String, Object> searchParams = new HashMap<String, Object>();
                String sn = command.getParams().get(0).get("sn").toString();
                searchParams.put("serialNo_eq", sn);
                Searchable searchable = Searchable.newSearchable(searchParams);
                Device device = deviceMap.get(sn);
                if (device == null) {
                    List<Device> devFind = deviceService.findAllWithSort(searchable);
                    if (devFind != null && devFind.size() > 0) {
                        device = devFind.get(0);
                        deviceMap.put(device.getSerialNo(), device);
                    }
                }

                IoSession tempIoSession = null;
                if (device != null) {
                    tempIoSession = ioSessionMap.get(device.getUserId() + "_" + device.getId());
                    logger.info("获取session标识=" + device.getUserId() + "_" + device.getId());
                }

                logger.info("获取浏览器session：" + tempIoSession);
                if (tempIoSession == null) {
                    //ioSessionMap.put(pcMac, session);
                    logger.info("Agent Image check session完毕。tempIoSession==null也许浏览器没有连接.");
                    logger.info("目前Image链接session个数:" + ioSessionMap.values().size());
                } else {
                    byte[] imgData = createImage(reqImage, command);// for test;
                    request.setImageContents(imgData);
                    // for test;
                    tempIoSession.write(request);
                    logger.info("发送到Image浏览器客户端:=" + message);
                }
            } else {
                //user id and device id save session;
                logger.info("Iamge server来自adapter,message=" + message);
                String uidDevid = command.getParams().get(0).get("user-deviceid").toString();
                logger.info("#UserID_devID=" + uidDevid);
                IoSession tempIoSession = ioSessionMap.get(uidDevid);
                if (tempIoSession == null) {
                    ioSessionMap.put(uidDevid, session);
                    session.setAttributeIfAbsent("uidDevid", uidDevid);
                    logger.info("放置浏览器session,用来写Image数据回去");
                } else {
                    logger.info("浏览器session已经存在。userDevid=" + uidDevid);
                }
            }

        } else {
            logger.info("Image中参数为空NULL,不处理。");
            return;
        }

    }

    @Override
    public void sessionIdle(IoSession session, IdleStatus status) throws Exception {
        logger.info("Image session IDLE " + session.getIdleCount(status));
    }

    @Override
    public void sessionClosed(IoSession session) throws Exception {
        String uidDevid = (String) session.getAttribute("uidDevid");
        ioSessionMap.remove(uidDevid);
        logger.info("session closed and remove session,uidDevid=" + uidDevid + ",session="
                    + session);
    }

    @Override
    public void messageSent(IoSession session, Object message) throws Exception {
        logger.info("Image服务端向客户端[" + session.getAttribute("uidDevid") + "]发送" + message + "信息成功");
    }

    public byte[] createImage(byte[] allData, Command command) {
        if (command == null) {
            return null;
        }
        if (allData == null || allData.length < 1) {
            return null;
        }
        System.out.println("接收到agent所有数据长度:" + allData.length);
        byte version = 0;
        byte header_length = 0;
        byte[] pid = new byte[4];
        byte[] width = new byte[4];
        byte[] height = new byte[4];
        byte[] vwidth = new byte[4];
        byte[] vheight = new byte[4];
        byte[] imgData = null;

        try {
            if ("header".equalsIgnoreCase(command.getParams().get(0).get("attribute").toString())) {
                logger.info("带head的图片");
                imgData = new byte[allData.length - 28];

                version = allData[0];
                header_length = allData[1];

                System.arraycopy(allData, 2, pid, 0, pid.length);
                System.arraycopy(allData, 6, width, 0, width.length);
                System.arraycopy(allData, 10, height, 0, height.length);
                System.arraycopy(allData, 14, vwidth, 0, vwidth.length);
                System.arraycopy(allData, 18, vheight, 0, vheight.length);
                System.arraycopy(allData, 28, imgData, 0, imgData.length);

                logger.info("#createImage version:" + version);
                logger.info("#createImage header_length:" + header_length);
                logger.info("#createImage pid:" + littleEndian2BigEndian(pid));
                logger.info("#createImage width:" + this.littleEndian2BigEndian(width));
                logger.info("#createImage height:" + this.littleEndian2BigEndian(height));
                logger.info("#createImage vwidth:" + this.littleEndian2BigEndian(vwidth));
                logger.info("#createImage vheight:" + this.littleEndian2BigEndian(vheight));
                //ImageIO.write(readImage(imgData), "PNG", new File("D:\\header.png"));
                return imgData;
            } else {
                logger.info("不带head的图片");
                if (allData.length - 4 > 0) {
                    imgData = new byte[allData.length - 4];
                    System.arraycopy(allData, 4, imgData, 0, imgData.length);
                }
                // ImageIO.write(readImage(imgData), "PNG", new File("D:\\headerno.png"));
                return imgData;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("错误的图片");
        return imgData;
    }

    public int littleEndian2BigEndian(byte[] w) {
        return (w[3]) << 24 | (w[2] & 0xff) << 16 | (w[1] & 0xff) << 8 | (w[0] & 0xff);
    }

    /*public BufferedImage readImage(byte[] in) throws IOException {
        ByteArrayInputStream bais = new ByteArrayInputStream(in);
        BufferedImage image = ImageIO.read(bais);
        bais.close();
        return image;
    }*/

    /* private String getIP(String remoteAdd) {
         if (remoteAdd == null) {
             return null;
         }
         if (remoteAdd.indexOf("/") < 0) {
             return null;
         }
         return StringUtil.substringBetween(remoteAdd, "/", ":");
     }*/
}
