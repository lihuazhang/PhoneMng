/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package com.jy.phonemng.biz.nettysocket;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jy.phonemng.biz.minaserver.CommandServer;
import com.jy.phonemng.biz.minaserver.ImageServer;

/**
 * 
 * @author kejun.song
 * @version $Id: NettyServlet.java, v 0.1 2015年8月19日 下午2:44:16 kejun.song Exp $
 */
public class NettyServlet extends HttpServlet {
    protected Logger          logger           = LoggerFactory.getLogger(NettyServlet.class);
    /**  */
    private static final long serialVersionUID = 1L;

    @Override
    public void init() throws ServletException {
        logger.info("开始启动socket服务端...");
        try {
            ImageServer.main(null);
            CommandServer.main(null);
        } catch (Exception e) {
            logger.error("WebSocketServer start error:", e);
        }
        logger.info("启动完毕socket服务端...");
    }

}
