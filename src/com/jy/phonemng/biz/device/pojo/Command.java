/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package com.jy.phonemng.biz.device.pojo;

import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 
 * @author kejun.song
 * @version $Id: Command.java, v 0.1 2015��8��27�� ����7:12:01 kejun.song Exp $
 */
public class Command {

    public String                         command = "";

    private List<HashMap<String, Object>> params;

    public Command() {

    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    /*public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }*/

    public List<HashMap<String, Object>> getParams() {
        return params;
    }

    public void setParams(List<HashMap<String, Object>> params) {
        this.params = params;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
