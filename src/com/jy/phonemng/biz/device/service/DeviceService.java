/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package com.jy.phonemng.biz.device.service;

import org.springframework.context.annotation.DependsOn;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jy.phonemng.biz.dal.model.Device;
import com.jy.phonemng.biz.device.repository.DeviceRepository;
import com.jy.phonemng.framework.base.BaseService;

/**
 * 
 * @author kejun.song
 * @version $Id: DeviceService.java, v 0.1 2015��8��5�� ����7:53:49 kejun.song Exp $
 */
@Service
@Transactional(readOnly = true)
@DependsOn(value = { "deviceRepository" })
public class DeviceService extends BaseService<Device, Long> {

    private DeviceRepository getLogRepository() {
        return (DeviceRepository) baseRepository;

    }

    public Page<Device> findByPage(Device device, Pageable pageable) {
        return getLogRepository().findAll(pageable);
    }

}
