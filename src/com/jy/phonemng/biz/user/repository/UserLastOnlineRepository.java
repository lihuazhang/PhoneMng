package com.jy.phonemng.biz.user.repository;

import com.jy.phonemng.biz.dal.model.UserLastOnline;
import com.jy.phonemng.framework.base.BaseRepository;

public interface UserLastOnlineRepository extends BaseRepository<UserLastOnline, Long> {

    UserLastOnline findByUserId(Long userId);
}
