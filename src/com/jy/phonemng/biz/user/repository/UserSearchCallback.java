package com.jy.phonemng.biz.user.repository;

import javax.persistence.Query;

import com.jy.phonemng.framework.callback.DefaultSearchCallback;
import com.jy.phonemng.framework.search.Searchable;

public class UserSearchCallback extends DefaultSearchCallback {

    public UserSearchCallback() {
        super("x");
    }

    @Override
    public void prepareQL(StringBuilder ql, Searchable search) {
        super.prepareQL(ql, search);

    }

    @Override
    public void setValues(Query query, Searchable search) {
        super.setValues(query, search);
    }

}
