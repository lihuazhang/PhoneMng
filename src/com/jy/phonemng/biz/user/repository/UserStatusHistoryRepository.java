package com.jy.phonemng.biz.user.repository;

import com.jy.phonemng.biz.dal.model.UserStatusHistory;
import com.jy.phonemng.framework.base.BaseRepository;

public interface UserStatusHistoryRepository extends BaseRepository<UserStatusHistory, Long> {

}
