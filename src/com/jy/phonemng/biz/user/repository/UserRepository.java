package com.jy.phonemng.biz.user.repository;

import com.jy.phonemng.biz.dal.model.User;
import com.jy.phonemng.framework.annotation.SearchableQuery;
import com.jy.phonemng.framework.base.BaseRepository;

@SearchableQuery(callbackClass = UserSearchCallback.class)
public interface UserRepository extends BaseRepository<User, Long> {

    User findByUserName(String userName);

    User findByMobilePhoneNumber(String mobilePhoneNumber);

    User findByEmail(String email);

    /*@Query("from UserOrganizationJob where user=?1 and organizationId=?2 and jobId=?3")
    Object findUserOrganization(User user, Long organizationId, Long jobId);

    @Query("select uoj from UserOrganizationJob uoj where not exists(select 1 from Job j where uoj.jobId=j.id) or not exists(select 1 from Organization o where uoj.organizationId=o.id)")
    Page<Object> findUserOrganizationJobOnNotExistsOrganizationOrJob(Pageable pageable);

    @Modifying
    @Query("delete from UserOrganizationJob uoj where not exists(select 1 from User u where uoj.user=u)")
    void deleteUserOrganizationJobOnNotExistsUser();*/
}
