/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package com.jy.phonemng.biz.user.service;

import org.springframework.aop.framework.AopContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.jy.phonemng.biz.dal.model.User;
import com.jy.phonemng.biz.user.repository.UserRepository;
import com.jy.phonemng.framework.annotation.UserStatus;
import com.jy.phonemng.framework.base.BaseService;
import com.jy.phonemng.sys.exception.UserBlockedException;
import com.jy.phonemng.sys.exception.UserNotExistsException;
import com.jy.phonemng.sys.exception.UserPasswordNotMatchException;

/**
 * 
 * @author kejun.song
 * @version $Id: UserService.java, v 0.1 2015年7月31日 下午2:36:03 kejun.song Exp $
 */
@Service
@Transactional(readOnly = true)
@DependsOn(value = { "userRepository" })
public class UserService extends BaseService<User, Long> {

    @Autowired
    protected PasswordService          passwordService;

    @Autowired
    protected UserStatusHistoryService userStatusHistoryService;

    @Autowired
    protected UserRepository getUserRepository() {
        return (UserRepository) baseRepository;
    }

    public User findByUserName(String username) {
        if (StringUtils.isEmpty(username)) {
            return null;
        }
        return getUserRepository().findByUserName(username);

    }

    public User login(String username, String password) {

        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password)) {
            logger.info(username, "loginError", "username is empty");
            throw new UserNotExistsException();
        }
        //密码如果不在指定范围内 肯定错误
        if (password.length() < User.PASSWORD_MIN_LENGTH
            || password.length() > User.PASSWORD_MAX_LENGTH) {
            logger.info(username, "loginError",
                "password length error! password is between {} and {}", User.PASSWORD_MIN_LENGTH,
                User.PASSWORD_MAX_LENGTH);

            throw new UserPasswordNotMatchException();
        }

        User user = null;

        //此处需要走代理对象，目的是能走缓存切面
        UserService proxyUserService = (UserService) AopContext.currentProxy();
        if (maybeUsername(username)) {
            user = proxyUserService.findByUserName(username);
        }

        if (user == null && maybeEmail(username)) {
            user = proxyUserService.findByEmail(username);
        }

        if (user == null && maybeMobilePhoneNumber(username)) {
            user = proxyUserService.findByMobilePhoneNumber(username);
        }

        if (user == null || Boolean.TRUE.equals(user.getDeleted())) {
            logger.info(username, "loginError", "user is not exists!");
            throw new UserNotExistsException();
        }

        passwordService.validate(user, password);

        if (user.getStatus() == UserStatus.blocked) {
            logger.info(username, "loginError", "user is blocked!");
            throw new UserBlockedException(userStatusHistoryService.getLastReason(user));
        }

        logger.info(username, "loginSuccess", "");
        return user;
    }

    private boolean maybeUsername(String username) {
        if (!username.matches(User.USERNAME_PATTERN)) {
            return false;
        }
        //如果用户名不在指定范围内也是错误的
        if (username.length() < User.USERNAME_MIN_LENGTH
            || username.length() > User.USERNAME_MAX_LENGTH) {
            return false;
        }

        return true;
    }

    private boolean maybeEmail(String username) {
        if (!username.matches(User.EMAIL_PATTERN)) {
            return false;
        }
        return true;
    }

    private boolean maybeMobilePhoneNumber(String username) {
        if (!username.matches(User.MOBILE_PHONE_NUMBER_PATTERN)) {
            return false;
        }
        return true;
    }

    public User findByEmail(String email) {
        if (StringUtils.isEmpty(email)) {
            return null;
        }
        return getUserRepository().findByEmail(email);

    }

    public User findByMobilePhoneNumber(String mobilePhoneNumber) {
        if (StringUtils.isEmpty(mobilePhoneNumber)) {
            return null;
        }
        return getUserRepository().findByMobilePhoneNumber(mobilePhoneNumber);
    }
}
