package com.jy.phonemng.biz.user.service;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.jy.phonemng.biz.dal.model.User;
import com.jy.phonemng.biz.dal.model.UserStatusHistory;
import com.jy.phonemng.biz.user.repository.UserStatusHistoryRepository;
import com.jy.phonemng.framework.annotation.UserStatus;
import com.jy.phonemng.framework.base.BaseService;
import com.jy.phonemng.framework.search.Searchable;

@Service
public class UserStatusHistoryService extends BaseService<UserStatusHistory, Long> {

    private UserStatusHistoryRepository getStatusHistoryRepository() {
        return (UserStatusHistoryRepository) baseRepository;
    }

    public void log(User opUser, User user, UserStatus newStatus, String reason) {
        UserStatusHistory history = new UserStatusHistory();
        history.setUser(user);
        history.setOpUser(opUser);
        history.setOpDate(new Date());
        history.setStatus(newStatus);
        history.setReason(reason);
        save(history);
    }

    public UserStatusHistory findLastHistory(final User user) {
        Searchable searchable = Searchable.newSearchable().addSearchParam("user_eq", user)
            .addSort(Sort.Direction.DESC, "opDate").setPage(0, 1);

        Page<UserStatusHistory> page = getStatusHistoryRepository().findAll(searchable);

        if (page.hasContent()) {
            return page.getContent().get(0);
        }
        return null;
    }

    public String getLastReason(User user) {
        UserStatusHistory history = findLastHistory(user);
        if (history == null) {
            return "";
        }
        return history.getReason();
    }
}
