package com.jy.phonemng.biz.user.service;

import org.springframework.stereotype.Service;

import com.jy.phonemng.biz.dal.model.UserLastOnline;
import com.jy.phonemng.framework.base.BaseService;

@Service
public class UserLastOnlineService extends BaseService<UserLastOnline, Long> {

    public UserLastOnline findByUserId(Long userId) {
        return null;//getUserLastOnlineRepository().findByUserId(userId);
    }

    public void lastOnline(UserLastOnline lastOnline) {
        UserLastOnline dbLastOnline = findByUserId(lastOnline.getUserId());

        if (dbLastOnline == null) {
            dbLastOnline = lastOnline;
        } else {
            UserLastOnline.merge(lastOnline, dbLastOnline);
        }
        dbLastOnline.incLoginCount();
        dbLastOnline.incTotalOnlineTime();
        //
        save(dbLastOnline);
    }
}
