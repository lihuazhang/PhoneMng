package com.jy.phonemng.biz.minacodec;

import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;
import org.apache.mina.filter.codec.demux.MessageEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ImageRequestEncoder implements MessageEncoder<AbstractMessage> {

    private static Logger logger = LoggerFactory.getLogger(ImageRequestEncoder.class);

    private final Charset charset;

    public ImageRequestEncoder(Charset charset) {
        this.charset = charset;
    }

    @Override
    public void encode(IoSession session, AbstractMessage message, ProtocolEncoderOutput out)
                                                                                             throws Exception {

        logger.info("###### encode start #####");
        IoBuffer buf = IoBuffer.allocate(100).setAutoExpand(true);
        buf.putShort(message.getTag());
        buf.putInt(message.getLen(charset));

        if (message instanceof ImageRequest) {
            ImageRequest req = (ImageRequest) message;
            CharsetEncoder encoder = charset.newEncoder();
            // 定义真实数据区
            IoBuffer dataBuffer = IoBuffer.allocate(100).setAutoExpand(true);
            // 偏移地址
            int offset = req.getDataOffset();
            // 文件名名称地址（偏移开始位置）
            buf.putInt(offset);
            byte from_len = 0;
            if (req.getFrom() != null) {
                from_len = (byte) req.getFrom().getBytes(charset).length;
            }
            buf.put(from_len);
            offset += from_len;
            // adapter名称
            if (from_len > 0) {
                dataBuffer.putString(req.getFrom(), encoder);
            }

            buf.putInt(offset);
            byte dataLength = 0;
            if (req.getData() != null) {
                dataLength = (byte) req.getData().getBytes(charset).length;
            }
            buf.put(dataLength);
            offset += dataLength;
            // adapter名称
            if (dataLength > 0) {
                dataBuffer.putString(req.getData(), encoder);
            }

            // 存放文件bytebuffer偏移地址
            buf.putInt(offset);
            long buffer_len = 0;

            // req.setFileContents(new);
            if (req.getImageContents() != null) {
                buffer_len = req.getImageContents().length;
            }

            buf.putLong(buffer_len);
            offset += buffer_len;
            // adapter名称
            if (buffer_len > 0) {
                byte[] bb = req.getImageContents();
                dataBuffer.put(bb);
                /*for (int i = 0; i < bb.limit(); i++) {
                    bb.position(i);
                    byte s = bb.get();
                    dataBuffer.put(s);
                }*/
            }

            // 真实数据追加在基本数据后面
            if (dataBuffer.position() > 0) {
                buf.put(dataBuffer.flip());
            }
        }
        // ==========编码成功=================
        buf.flip();
        out.write(buf);
        logger.info("###### encode end #####");
    }

}
