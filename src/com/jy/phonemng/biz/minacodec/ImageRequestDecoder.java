package com.jy.phonemng.biz.minacodec;

import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.apache.mina.filter.codec.demux.MessageDecoder;
import org.apache.mina.filter.codec.demux.MessageDecoderResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ImageRequestDecoder implements MessageDecoder {

    private static Logger logger = LoggerFactory.getLogger(ImageRequestDecoder.class);
    private final Charset charset;

    public ImageRequestDecoder(Charset charset) {
        this.charset = charset;
    }

    @Override
    public MessageDecoderResult decodable(IoSession session, IoBuffer in) {
        // 报头长度==6
        if (in.remaining() < 6) {
            return MessageDecoderResult.NEED_DATA;
        }

        // tag正常
        short tag = in.getShort();
        // 注意先把16进制标识值转换为short类型的十进制数据，然后与tag比较
        if (tag == (short) 0x0001) {
            // logger.info("请求标识符：" + tag);
        } else {
            logger.error("未知的解码类型....tag=" + tag);
            return MessageDecoderResult.NOT_OK;
        }

        // 真实数据长度
        int len = in.getInt();
        if (in.remaining() < len) {
            return MessageDecoderResult.NEED_DATA;
        }

        return MessageDecoderResult.OK;
    }

    @Override
    public MessageDecoderResult decode(IoSession session, IoBuffer in, ProtocolDecoderOutput out)
                                                                                                 throws Exception {

        CharsetDecoder decoder = charset.newDecoder();
        AbstractMessage message = null;
        short tag = in.getShort(); // tag
        int len = in.getInt(); // len

        byte[] temp = new byte[len];
        in.get(temp); // 数据区

        // ===============解析数据做准备======================
        IoBuffer buf = IoBuffer.allocate(100).setAutoExpand(true);
        buf.put(temp);
        buf.flip(); // 为获取基本数据区长度做准备

        IoBuffer databuf = IoBuffer.allocate(100).setAutoExpand(true);
        databuf.putShort(tag);
        databuf.putInt(len);
        databuf.put(temp);
        databuf.flip(); // 为获取真实数据区长度做准备

        if (tag == (short) 0x0001) {
            //          logger.info("信息:0x1002");
            ImageRequest req = new ImageRequest();

            int from_addr = buf.getInt();
            byte from_len = buf.get();
            if (databuf.position() == 0) {
                databuf.position(from_addr);
            }

            String from = null;
            if (from_len > 0) {
                from = databuf.getString(from_len, decoder);
            }
            System.out.println("## image requst decode:from=" + from);
            int data_addr = buf.getInt();
            byte data_len = buf.get();
            if (databuf.position() == 0) {
                databuf.position(data_addr);
            }

            String data = null;
            if (data_len > 0) {
                data = databuf.getString(data_len, decoder);
            }
            System.out.println("## image requst decode:data=" + data);
            int file_addr = buf.getInt();
            long file_len = buf.getLong();
            byte[] imageBuffer = new byte[(int) file_len];
            databuf.position(file_addr);
            databuf.get(imageBuffer);

            req.setFrom(from);
            req.setData(data);
            // ByteBuffer tmBuf = ByteBuffer.wrap(imageBuffer);
            req.setImageContents(imageBuffer);

            message = req;

        }
        out.write(message);
        // ================解码成功=========================
        //databuf.clear();
        // buf.clear();
        //logger.info("Decode清理成功。");
        return MessageDecoderResult.OK;
    }

    @Override
    public void finishDecode(IoSession arg0, ProtocolDecoderOutput arg1) throws Exception {
        // TODO Auto-generated method stub

    }

}
