/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package com.jy.phonemng.biz.minacodec;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author kejun.song
 * @version $Id: CommandDecoder.java, v 0.1 2015��9��2�� ����4:47:44 kejun.song Exp $
 */
public class CommandDecoder extends CumulativeProtocolDecoder {

    private static Logger logger = LoggerFactory.getLogger(CommandDecoder.class);

    @Override
    protected boolean doDecode(IoSession session, IoBuffer bufferIn, ProtocolDecoderOutput out)
                                                                                               throws Exception {
        logger.info("Command decode start. remain=" + bufferIn.remaining());
        int cmdLen = bufferIn.getInt();
        byte[] cmdByte = new byte[cmdLen];
        bufferIn.get(cmdByte);
        String cmdStr = new String(cmdByte);

        out.write(cmdStr);
        logger.info("Command decode end. remain=" + bufferIn.remaining());
        return true;
    }

}
