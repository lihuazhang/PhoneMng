package com.jy.phonemng.biz.minacodec;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolEncoder;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author kejun.song
 * @version $Id: CommandEncoder.java, v 0.1 2015��9��2�� ����4:39:42 kejun.song Exp $
 */
public class CommandEncoder implements ProtocolEncoder {

    private static Logger logger = LoggerFactory.getLogger(CommandEncoder.class);

    @Override
    public void dispose(IoSession arg0) throws Exception {
    }

    @Override
    public void encode(IoSession session, Object msg, ProtocolEncoderOutput out) throws Exception {
        logger.info("command encode start.");
        String cmd = (String) msg;
        int cmdLen = cmd.getBytes().length;
        int capacity = cmdLen + 4;

        IoBuffer buffer = IoBuffer.allocate(capacity).setAutoExpand(true);

        buffer.putInt(cmdLen);
        buffer.put(cmd.getBytes());

        buffer.flip();
        out.write(buffer);
        logger.info("command encode end.");
    }

}
