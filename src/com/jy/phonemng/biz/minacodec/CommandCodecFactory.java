package com.jy.phonemng.biz.minacodec;

import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFactory;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolEncoder;

/**
 * 
 * @author kejun.song
 * @version $Id: CommandCodecFactory.java, v 0.1 2015��9��2�� ����4:55:49 kejun.song Exp $
 */
public class CommandCodecFactory implements ProtocolCodecFactory {

    private final ProtocolEncoder encoder;
    private final ProtocolDecoder decoder;

    public CommandCodecFactory() {
        encoder = new CommandEncoder();
        decoder = new CommandDecoder();
    }

    @Override
    public ProtocolDecoder getDecoder(IoSession arg0) throws Exception {
        return decoder;
    }

    @Override
    public ProtocolEncoder getEncoder(IoSession arg0) throws Exception {
        return encoder;
    }

}
