package com.jy.phonemng.biz.minacodec;

import java.nio.charset.Charset;

/**
 * represents a client's request for an image
 *
 * 
 */

public class ImageRequest extends AbstractMessage {

    private String from = "web";
    private String data = "default";

    private byte[] imageContents;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public byte[] getImageContents() {
        return imageContents;
    }

    public void setImageContents(byte[] imageContents) {
        this.imageContents = imageContents;
    }

    @Override
    public short getTag() {
        return 0x0001; //tag send image request
    }

    @Override
    public int getLen(Charset charset) {
        int len = 4// 
                  + 1 + 4 + 1 + 4 + 8;
        if (from != null && !"".equals(from)) {
            len += from.getBytes(charset).length;
        }

        if (data != null && !"".equals(data)) {
            len += data.getBytes(charset).length;
        }

        if (imageContents != null) {
            len += imageContents.length;
        }
        return len;
    }

    @Override
    public int getDataOffset() {
        //tag+total.length+filename.offset+filename.length+offset+data.length
        int len = 2 //tag
                  + 4 //total length
                  + 4 //filename.offset
                  + 1 //filename.length
                  + 4 //user.offset
                  + 1 //user.length
                  + 4 //file.offset
                  + 8;//file.length
        return len;
    }

    @Override
    public String toString() {
        return "data=" + this.data + "from=" + from;
    }
}
