package com.jy.phonemng.biz.minacodec;


/**
 * response sent by the server when receiving an {@link ImageRequest}
 *
 *
 */
public class ImageResponse {

    /**  */
    // private static final long serialVersionUID = 1L;

    private byte[] image = null;
    private String flag  = "";

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public ImageResponse(byte[] image) {
        this.image = image;
    }

    public byte[] getImage() {
        return image;
    }

    @Override
    public String toString() {
        return "data=" + this.flag;
    }
}
