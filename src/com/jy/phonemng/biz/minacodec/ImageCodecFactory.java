package com.jy.phonemng.biz.minacodec;

import org.apache.mina.filter.codec.demux.DemuxingProtocolCodecFactory;
import org.apache.mina.filter.codec.demux.MessageDecoder;
import org.apache.mina.filter.codec.demux.MessageEncoder;

public class ImageCodecFactory extends DemuxingProtocolCodecFactory {
    private final MessageDecoder                  decoder;
    private final MessageEncoder<AbstractMessage> encoder;

    public ImageCodecFactory(MessageDecoder decoder, MessageEncoder<AbstractMessage> encoder) {
        this.decoder = decoder;
        this.encoder = encoder;
        addMessageDecoder(this.decoder);
        addMessageEncoder(AbstractMessage.class, this.encoder);
    }
}
