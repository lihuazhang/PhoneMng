package com.jy.phonemng.biz.minaclient;

public interface ImageListener {

    void onImages(byte[] image1);

    void onCommand(String command);

    void onException(Throwable throwable);

    void sessionOpened();

    void sessionClosed();
}
