/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package com.jy.phonemng.biz.minaclient;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.jy.phonemng.biz.minacodec.ImageRequest;

/**
 * 
 * @author kejun.song
 * @version $Id: ProxyImageClient.java, v 0.1 2015��8��21�� ����5:20:38 kejun.song Exp $
 */
public class ProxyImageClient implements ImageListener {
    public static final int    PORT        = 33789;
    public static final String HOST        = "localhost";

    private final ImageClient  imageClient = new ImageClient(HOST, PORT, this);

    /**
     * 
     * @param args
     */
    public static void main(String[] args) {

        new ProxyImageClient().sendRequest();

    }

    @Override
    public void onImages(byte[] image1) {
        //sendRequest();
        //imagePanel1.setImages(image1, image2);
    }

    @Override
    public void onException(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void sessionOpened() {
        System.out.println("sesseion opened.");
    }

    /** 
     * @see com.jy.phonemng.biz.minaclient.ImageListener#sessionClosed()
     */
    @Override
    public void sessionClosed() {
        System.out.println("session closed.");
    }

    private void sendRequest() {
        if (imageClient != null) {
            if (!imageClient.isConnected()) {
                imageClient.connect();
            }
        }

        try {
            FileInputStream imageInputStream = new FileInputStream("D:\\JavaScript\\leftphone.png");
            BufferedImage image = ImageIO.read(imageInputStream);

            //byte[] image2 = new byte[] { 0x01, 0x02, 0x03, 0x04 };
            ImageRequest imageRequest = new ImageRequest();
            imageRequest.setFrom("agent");
            String data = "{\"command\":\"IMAGE\",\"params\":[{\"sn\":\"cfec0c6e\",\"attribute\":\"body\",\"from\":\"agent\"}]}";
            imageRequest.setData(data);
            imageInputStream.close();
            int i = 2000;
            while (i > 0) {
                imageClient.sendRequest(imageRequest);
                Thread.sleep(200);
                System.out.println("******* i=" + i);
                i--;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private byte[] getBytes(BufferedImage image) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(image, "PNG", baos);
        return baos.toByteArray();
    }

    @Override
    public void onCommand(String command) {
    }

}
