package com.jy.phonemng.biz.minaclient;

import java.net.InetSocketAddress;
import java.nio.charset.Charset;

import org.apache.mina.core.RuntimeIoException;
import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.transport.socket.SocketConnector;
import org.apache.mina.transport.socket.nio.NioSocketConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jy.phonemng.biz.minacodec.ImageCodecFactory;
import com.jy.phonemng.biz.minacodec.ImageRequest;
import com.jy.phonemng.biz.minacodec.ImageRequestDecoder;
import com.jy.phonemng.biz.minacodec.ImageRequestEncoder;

public class ImageClient extends IoHandlerAdapter {

    public static final int       CONNECT_TIMEOUT = 3000;

    private static Logger         logger          = LoggerFactory.getLogger(ImageClient.class);

    private final String          host;
    private final int             port;
    private final SocketConnector connector;
    private IoSession             session;
    private final ImageListener   imageListener;

    //private final static int      maxNioCache     = 50 * 1024 * 1024;

    public ImageClient(String host, int port, ImageListener imageListener) {
        this.host = host;
        this.port = port;
        this.imageListener = imageListener;
        connector = new NioSocketConnector();
        connector.getFilterChain().addLast(
            "codec",
            new ProtocolCodecFilter(new ImageCodecFactory(new ImageRequestDecoder(Charset
                .forName("utf-8")), new ImageRequestEncoder(Charset.forName("utf-8")))));

        /*ObjectSerializationCodecFactory objSFactory = new ObjectSerializationCodecFactory();
        objSFactory.setDecoderMaxObjectSize(maxNioCache);
        objSFactory.setEncoderMaxObjectSize(maxNioCache);
        connector.getFilterChain().addLast("mychain", new ProtocolCodecFilter(objSFactory));*/

        connector.setHandler(this);
    }

    public boolean isConnected() {
        return (session != null && session.isConnected());
    }

    public void connect() {
        ConnectFuture connectFuture = connector.connect(new InetSocketAddress(host, port));
        connectFuture.awaitUninterruptibly(CONNECT_TIMEOUT);
        logger.info("重新连接session.");
        try {
            session = connectFuture.getSession();
            logger.info("定义属性:websocketSession=" + session.getAttribute("websocketSession"));
        } catch (RuntimeIoException e) {
            imageListener.onException(e);
        }
    }

    public void disconnect() {
        if (session != null) {
            session.close(true);
            session = null;
            connector.dispose();
        }
    }

    @Override
    public void sessionOpened(IoSession session) throws Exception {
        imageListener.sessionOpened();
    }

    @Override
    public void sessionClosed(IoSession session) throws Exception {
        imageListener.sessionClosed();
    }

    public void sendRequest(ImageRequest imageRequest) {
        if (session == null) {
            //imageListener.onException(new Throwable("not connected"));
            connect();
            logger.info("向客户端发送数据:" + imageRequest.getData());
            session.write(imageRequest);
        } else {
            logger.info("向客户端发送数据:" + imageRequest.getData());
            session.write(imageRequest);
        }
    }

    @Override
    public void messageReceived(IoSession session, Object message) throws Exception {
        logger.info("#Image client received image=" + message);
        ImageRequest response = (ImageRequest) message;
        imageListener.onImages(response.getImageContents());
    }

    @Override
    public void exceptionCaught(IoSession session, Throwable cause) throws Exception {
        imageListener.onException(cause);
    }

    /* private BufferedImage readImage(byte[] in) throws IOException {
         ByteArrayInputStream bais = new ByteArrayInputStream(in);
         BufferedImage image = ImageIO.read(bais);
         bais.close();
         return image;
     }*/
}
