/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package com.jy.phonemng.biz.minaclient;

import java.net.InetSocketAddress;
import java.nio.charset.Charset;

import org.apache.mina.core.RuntimeIoException;
import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.textline.LineDelimiter;
import org.apache.mina.filter.codec.textline.TextLineCodecFactory;
import org.apache.mina.transport.socket.SocketConnector;
import org.apache.mina.transport.socket.nio.NioSocketConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author kejun.song
 * @version $Id: CommandClient.java, v 0.1 2015年9月2日 下午7:05:00 kejun.song Exp $
 */
public class CommandClient extends IoHandlerAdapter {

    public static final int       CONNECT_TIMEOUT = 3000;

    private static Logger         logger          = LoggerFactory.getLogger(ImageClient.class);

    private final String          host;
    private final int             port;
    private final SocketConnector connector;
    private IoSession             session;
    private final ImageListener   imageListener;

    //private final static int      maxNioCache     = 50 * 1024 * 1024;

    public CommandClient(String host, int port, ImageListener imageListener) {
        this.host = host;
        this.port = port;
        this.imageListener = imageListener;
        connector = new NioSocketConnector();

        TextLineCodecFactory factory = new TextLineCodecFactory(Charset.forName("UTF-8"),
            LineDelimiter.WINDOWS.getValue(), LineDelimiter.WINDOWS.getValue());
        connector.getFilterChain().addLast("codec", new ProtocolCodecFilter(factory));

        connector.setHandler(this);
    }

    public boolean isConnected() {
        return (session != null && session.isConnected());
    }

    public void connect() {
        ConnectFuture connectFuture = connector.connect(new InetSocketAddress(host, port));
        connectFuture.awaitUninterruptibly(CONNECT_TIMEOUT);
        logger.info("重新连接session.");
        try {
            session = connectFuture.getSession();

        } catch (RuntimeIoException e) {
            imageListener.onException(e);
        }
    }

    public void disconnect() {
        if (session != null) {
            session.close(true);
            session = null;
            //connector.dispose();
        }
    }

    @Override
    public void sessionOpened(IoSession session) throws Exception {
        imageListener.sessionOpened();
    }

    @Override
    public void sessionClosed(IoSession session) throws Exception {
        imageListener.sessionClosed();
    }

    public void sendRequest(String command) {
        if (session == null) {
            //imageListener.onException(new Throwable("not connected"));
            connect();
            System.out.println("向客户端发送命令:" + command);
            session.write(command);
        } else {

            System.out.println("向客户端发送命令:" + command);
            session.write(command);
        }
    }

    @Override
    public void messageReceived(IoSession session, Object message) throws Exception {
        System.out.println("command client received message from server." + message);
        String response = (String) message;
        imageListener.onCommand(response);

    }

    @Override
    public void exceptionCaught(IoSession session, Throwable cause) throws Exception {
        imageListener.onException(cause);
    }

}
