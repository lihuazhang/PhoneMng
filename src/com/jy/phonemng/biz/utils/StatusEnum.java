/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package com.jy.phonemng.biz.utils;

/**
 * 
 * @author kejun.song
 * @version $Id: PlanEnum.java, v 0.1 2015��5��15�� ����5:02:31 kejun.song Exp $
 */
public enum StatusEnum {

    RUNING("R", "Running"), //running
    STOP("P", "stoped"), //stop
    SUCCESS("S", "Success"), //ss
    FAIL("F", "Running failed."), //
    BUSY("BUSY", "Device busy"), //
    FREE("FREE", "Device free"), //
    OFFLINE("OFFLINE", "Device offline"), //
    ONLINE("ONLINE", "Device online");//

    private StatusEnum(String value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    private String value;
    private String desc;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
