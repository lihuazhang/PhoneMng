package com.jy.phonemng.biz.utils;

import java.util.Map;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.UnavailableSecurityManagerException;
import org.apache.shiro.session.InvalidSessionException;

import com.google.common.collect.Maps;
import com.jy.phonemng.biz.dal.model.User;
import com.jy.phonemng.framework.base.BaseService;

public class UserUtils extends BaseService {

    public static final String CACHE_USER        = "user";
    public static final String CACHE_ROLE_LIST   = "roleList";
    public static final String CACHE_MENU_LIST   = "menuList";
    public static final String CACHE_AREA_LIST   = "areaList";
    public static final String CACHE_OFFICE_LIST = "officeList";

    public static User getUser() {
        User user = (User) getCache(CACHE_USER);

        if (user == null) {
            user = new User();
            try {
                SecurityUtils.getSubject().logout();
            } catch (UnavailableSecurityManagerException e) {

            } catch (InvalidSessionException e) {

            }
        }
        return user;
    }

    public static User getUser(boolean isRefresh) {
        if (isRefresh) {
            removeCache(CACHE_USER);
        }
        return getUser();
    }

    // ============== User Cache ==============

    public static Object getCache(String key) {
        return getCache(key, null);
    }

    public static Object getCache(String key, Object defaultValue) {
        Object obj = getCacheMap().get(key);
        return obj == null ? defaultValue : obj;
    }

    public static void putCache(String key, Object value) {
        getCacheMap().put(key, value);
    }

    public static void removeCache(String key) {
        getCacheMap().remove(key);
    }

    public static Map<String, Object> getCacheMap() {
        Map<String, Object> map = Maps.newHashMap();

        return map;
    }

}
