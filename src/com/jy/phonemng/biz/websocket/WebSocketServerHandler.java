/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package com.jy.phonemng.biz.websocket;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.BinaryWebSocketHandler;

import com.jy.phonemng.biz.dal.model.Device;
import com.jy.phonemng.biz.device.service.DeviceService;
import com.jy.phonemng.biz.utils.StatusEnum;
import com.jy.phonemng.framework.utils.SpringUtils;

/**
 * 
 * @author kejun.song
 * @version $Id: SystemWebSocketHandler.java, v 0.1 2015年8月14日 下午8:00:40 kejun.song Exp $
 */
public class WebSocketServerHandler extends BinaryWebSocketHandler implements WebSocketHandler {

    protected Logger                                                 logger        = LoggerFactory
                                                                                       .getLogger(WebSocketServerHandler.class);

    protected DeviceService                                          deviceService = SpringUtils
                                                                                       .getBean(DeviceService.class);

    // private WebSocketClientAdaptor                             clientAdaptor = null;

    private static ConcurrentHashMap<Object, WebSocketSession>       webSocketMap  = new ConcurrentHashMap<Object, WebSocketSession>();

    private static ConcurrentHashMap<Object, WebSocketClientAdaptor> adaptorMap    = new ConcurrentHashMap<Object, WebSocketClientAdaptor>();

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus arg1) throws Exception {
        logger.info("==>>websocket connection closed......");
        String queryString = session.getUri().getQuery();
        //webSocketMap.remove(key);
        logger.info("websocketQueryString:" + queryString);
        String sessionId = session.getId();
        webSocketMap.remove(sessionId + "_" + queryStringToMap(queryString).get("id"));
        logger.info("->>afterConnectionClosed清楚session:" + sessionId + "_"
                    + queryStringToMap(queryString).get("id"));
        logger.info("->>afterConnectionClosed剩下session个数:" + webSocketMap.values().size());
        String uid_devid = (String) session.getAttributes().get("user-deviceid");
        if (uid_devid != null) {
            String[] devidArray = uid_devid.split("_");
            String deviceId = devidArray[1];
            Device device = deviceService.findOne(Long.valueOf(deviceId));
            if (device != null) {
                device.setStatus(StatusEnum.FREE.getValue());
                deviceService.update(device);
                logger.info("Websocket设置设备空闲.");
            }

        }
        WebSocketClientAdaptor clientAdaptor = adaptorMap.get("adaptor_"
                                                              + sessionId
                                                              + "_"
                                                              + queryStringToMap(queryString).get(
                                                                  "id"));
        if (clientAdaptor != null) {
            clientAdaptor.sendRequest("STOPVIDEO");
            logger.info("==>>Websocket关闭,释放设备。");
            clientAdaptor.sessionClosed();
            adaptorMap.remove("adaptor_" + sessionId + "_"
                              + queryStringToMap(queryString).get("id"));
            logger.info("->>adaptorMap剩下个数:" + adaptorMap.values().size());
        }
        session.close();
        logger.info("=====WebsocketSession closed.====");
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        logger.info("==>>connect to the websocket success......");
        logger.info("=>>websocketEstablished建立连接：" + session);
        String queryString = session.getUri().getQuery();
        logger.info("websocketQueryString:" + queryString);
        String sessionId = session.getId();
        if (!queryString.contains("totalTimeOut")) {
            webSocketMap.put(sessionId + "_" + queryStringToMap(queryString).get("id"), session);
        }

        logger.info("->>websocketKEY:" + sessionId + "_" + queryStringToMap(queryString).get("id")
                    + "<<->>websocketSession个数:" + webSocketMap.values().size());
        WebSocketClientAdaptor clientAdaptor = new WebSocketClientAdaptor();
        clientAdaptor.setSession(session);
        clientAdaptor.setSocketSeesionMap(webSocketMap);
        adaptorMap.put("adaptor_" + sessionId + "_" + queryStringToMap(queryString).get("id"),
            clientAdaptor);
        logger.info("->>adaptorMapKEY:" + "adaptor_" + sessionId + "_"
                    + queryStringToMap(queryString).get("id") + "<<->>adaptor个数:"
                    + adaptorMap.values().size());
    }

    @Override
    public void handleMessage(WebSocketSession session, WebSocketMessage<?> message)
                                                                                    throws Exception {
        logger.info("==>>websocket接收消息from browser=" + message);
        logger.info("=>>websocket建立连接:session=" + session);
        String queryString = session.getUri().getQuery();
        System.out.println("websocketQueryString:" + queryString);
        logger.info("==>>websocket接收消息payload :" + message.getPayload() + " received at server");
        String payload = (String) message.getPayload();
        try {
            String deviceID = queryStringToMap(queryString).get("id");
            String sessionId = session.getId();
            WebSocketSession socketSession = webSocketMap.get(sessionId + "_" + deviceID);
            logger.info("->>获取websocketsessionKey:" + sessionId + "_" + deviceID);
            logger.info("->>获取adaptor_Key:" + "adaptor_" + sessionId + "_"
                        + queryStringToMap(queryString).get("id"));
            WebSocketClientAdaptor clientAdaptor = adaptorMap.get("adaptor_"
                                                                  + sessionId
                                                                  + "_"
                                                                  + queryStringToMap(queryString)
                                                                      .get("id"));
            if (clientAdaptor != null) {
                if (socketSession != null) {
                    clientAdaptor.setSession(socketSession);
                    logger.info("->>从map中获取socketsession.");
                } else {
                    clientAdaptor.setSession(session);
                    logger.info("->>用默认socketsession.");
                }
                clientAdaptor.sessionOpened();
                if (payload.indexOf("pointer") >= 0) {
                    String[] point = payload.split("_");
                    clientAdaptor.sendRequest(payload);
                    logger.info("==>>鼠标点击发送坐标到Minaserver:x=" + point[2] + ",y=" + point[3]);
                    // }
                } else if (payload.indexOf("key") >= 0) {//menu;
                    clientAdaptor.sendRequest(payload);
                } else if (!"loop".equalsIgnoreCase(payload)) {
                    logger.info("浏览器打开初始化请求图像");
                    clientAdaptor.sendRequest("STARTVIDEO");
                }
            } else {
                logger.info("->>clientAdaptor is NULL.");
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("******** Error ******** " + e.getMessage(), e);
        }

    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable arg1) throws Exception {
        logger.info("##handleTransportError connection closed......");
        if (session != null && session.isOpen()) {
            //session.close(CloseStatus.SERVER_ERROR);
            logger.info("==== session closed by error.====");
        }

    }

    @Override
    public boolean supportsPartialMessages() {
        return false;
    }

    public Map<String, String> queryStringToMap(String queryString) {
        String[] q = queryString.split("&");
        Map<String, String> queryMap = new HashMap<String, String>();
        for (int i = 0; i < q.length; i++) {
            if (q[i] != null) {
                String[] m = q[i].split("=");
                if (m != null && m.length == 2) {
                    queryMap.put(m[0], m[1]);
                }
            }
        }
        return queryMap;
    }
}
