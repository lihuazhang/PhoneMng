/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package com.jy.phonemng.biz.websocket;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.support.HttpSessionHandshakeInterceptor;

/**
 * 
 * @author kejun.song
 * @version $Id: WebSocketHandshakeInterceptor.java, v 0.1 2015年8月14日 下午8:09:46 kejun.song Exp $
 */
public class WebSocketHandshakeInterceptor extends HttpSessionHandshakeInterceptor {
    protected Logger logger = LoggerFactory.getLogger(WebSocketHandshakeInterceptor.class);

    @Override
    public void afterHandshake(ServerHttpRequest request, ServerHttpResponse response,
                               WebSocketHandler wsHandler, Exception ex) {
        logger.info("#afterHandshake request=" + request);

        super.afterHandshake(request, response, wsHandler, ex);
    }

    @Override
    public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse response,
                                   WebSocketHandler wsHandler, Map<String, Object> attributes)
                                                                                              throws Exception {
        ServletServerHttpRequest servletRequest = (ServletServerHttpRequest) request;
        logger.info("#beforeHandshake request=" + request);
        HttpSession session = servletRequest.getServletRequest().getSession(false);
        if (session != null) {

            if (attributes.get("userName") == null) {
                attributes.put("userName", "AncinTest");
            }

        }
        logger.info("#beforeHandshake获取webSocket中user-deviceid:"
                    + session.getAttribute("user-deviceid"));
        attributes.put("user-deviceid", session.getAttribute("user-deviceid"));

        return super.beforeHandshake(request, response, wsHandler, attributes);
    }
}
