package com.jy.phonemng.biz.websocket;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.WebSocketSession;

import com.jy.phonemng.biz.dal.model.Device;
import com.jy.phonemng.biz.device.pojo.Command;
import com.jy.phonemng.biz.device.service.DeviceService;
import com.jy.phonemng.biz.minaclient.CommandClient;
import com.jy.phonemng.biz.minaclient.ImageClient;
import com.jy.phonemng.biz.minaclient.ImageListener;
import com.jy.phonemng.biz.minacodec.ImageRequest;
import com.jy.phonemng.biz.utils.StatusEnum;
import com.jy.phonemng.framework.utils.JacksonUtils;
import com.jy.phonemng.framework.utils.SpringUtils;

/**
 * 
 * @author kejun.song
 * @version $Id: WebSocketAdaptor.java, v 0.1 2015年8月25日 上午10:03:15 kejun.song Exp $
 */
public class WebSocketClientAdaptor implements ImageListener {
    protected Logger                                    logger      = LoggerFactory
                                                                        .getLogger(WebSocketClientAdaptor.class);

    private WebSocketSession                            webSession;

    public static final int                             PORT        = 33789;
    public static final int                             PORT2       = 33788;

    public static final String                          HOST        = "localhost";

    private final ImageClient                           imageClient = new ImageClient(HOST, PORT,
                                                                        this);

    private final CommandClient                         cmdClient   = new CommandClient(HOST,
                                                                        PORT2, this);

    // private ArrayList<WebSocketSession>              webSessionList;
    private ConcurrentHashMap<Object, WebSocketSession> socketSeesionMap;

    public ConcurrentHashMap<Object, WebSocketSession> getSocketSeesionMap() {
        return socketSeesionMap;
    }

    public void setSocketSeesionMap(ConcurrentHashMap<Object, WebSocketSession> socketSeesionMap) {
        this.socketSeesionMap = socketSeesionMap;
    }

    protected DeviceService                          deviceService = SpringUtils
                                                                       .getBean(DeviceService.class);

    private static ConcurrentHashMap<Object, Device> deviceMap     = new ConcurrentHashMap<Object, Device>();

    public WebSocketSession getSession() {
        return webSession;
    }

    public void setSession(WebSocketSession session) {
        this.webSession = null;
        this.webSession = session;
    }

    public WebSocketClientAdaptor() {

    }

    public WebSocketClientAdaptor(WebSocketSession session) {
        this.webSession = session;
    }

    @Override
    public void onImages(byte[] image1) {
        logger.info("==>>Adaptor listener on images.");
        BinaryMessage binaryMessage = new BinaryMessage(image1);
        logger.info("=>>onImages每次的webSeesion值:" + webSession);
        try {
            synchronized (webSession) {//解决小写来不及接收的问题
                if (webSession != null && webSession.isOpen()) {
                    webSession.sendMessage(binaryMessage);
                    logger.info("==>>向浏览器发送数据-->...");
                } else {
                    logger.info("Websocket已经关闭!");
                    /* if (socketSeesionMap != null && socketSeesionMap.size() > 0) {
                         String queryString = webSession.getUri().getQuery();
                         webSession = socketSeesionMap.get(queryStringToMap(queryString).get("id"));
                         webSession.sendMessage(binaryMessage);
                         logger.info("==>>向浏览器发送Image数据-->...");
                     }*/
                }
            }//synchronized end;

        } catch (IOException e) {
            logger.error("websocket发送消息错误:", e);
        }
    }

    @Override
    public void onException(Throwable throwable) {
        logger.error("Adaptor error:", throwable);
    }

    @Override
    public void sessionOpened() {
        logger.info("adaptor session opened.");
        if (imageClient != null) {
            if (!imageClient.isConnected()) {
                imageClient.connect();
                logger.info("===>>websocket server minaclient connected.===");
            }
        }
        if (cmdClient != null) {
            if (!cmdClient.isConnected()) {
                cmdClient.connect();
                logger.info("===>>cmd server minaclient connected.===");
            }
        }
    }

    @Override
    public void sessionClosed() {
        logger.info("adaptor session closed.");
        webSession = null;
        imageClient.disconnect();
        cmdClient.disconnect();
    }

    public void sendRequest(String payload) {
        logger.info("=>>sendRequest每次的webSeesion值:" + webSession);
        String uid_devid = (String) webSession.getAttributes().get("user-deviceid");
        Device device = null;
        if (uid_devid != null) {
            String[] devidArray = uid_devid.split("_");
            String deviceId = devidArray[1];
            device = deviceMap.get(deviceId);
            //放置缓存
            if (device == null) {
                device = deviceService.findOne(Long.valueOf(deviceId));
                deviceMap.put(deviceId, device);
            }

        }
        if (payload != null) {
            if (payload.indexOf("pointer") >= 0) {
                String[] point = payload.split("_");
                if (imageClient != null) {
                    ImageRequest imageRequest = new ImageRequest();//baos.toByteArray()
                    //String uidDevid = webSession.getAttributes().get("user-deviceid").toString();
                    Command cmd = new Command();
                    List<HashMap<String, Object>> params = new ArrayList<HashMap<String, Object>>();
                    HashMap<String, Object> map = new HashMap<String, Object>();
                    map.put("user-deviceid", webSession.getAttributes().get("user-deviceid"));
                    map.put("from", "web");
                    map.put("sn", device.getSerialNo());
                    params.add(map);
                    cmd.setParams(params);

                    String command = JacksonUtils.beanToJson(cmd);

                    imageRequest.setData(command);
                    imageRequest.setFrom("web");
                    /***
                     * 浏览器通过websocket和Mina建立连接
                     */
                    imageClient.sendRequest(imageRequest);
                    /****
                     * 命令客户端和Mina简历链接，方便马上通讯。
                     */
                    String commandJson = null;

                    if (point[1].equalsIgnoreCase("0")) {
                        commandJson = createCmd(point[2], point[3], device, "touchDown");
                        cmdClient.sendRequest(commandJson);
                        logger.info("==>>CMD和Image client建立连接。touchDown");
                    } else if (point[1].equalsIgnoreCase("1")) {
                        commandJson = createCmd(point[2], point[3], device, "touchUp");
                        cmdClient.sendRequest(commandJson);
                        logger.info("==>>CMD和Image client建立连接。touchUp");
                    } else if (point[1].equalsIgnoreCase("2")) {
                        commandJson = createCmd(point[2], point[3], device, "touchMove");
                        cmdClient.sendRequest(commandJson);
                        logger.info("==>>CMD和Image client建立连接。touchMove");
                    }

                }
            } else if (payload.indexOf("key") >= 0) {
                String[] point = payload.split("_");
                Command cmd = new Command();
                if (point.length == 3) {
                    if ("4".equalsIgnoreCase(point[2])) {
                        cmd.setCommand("pressBack");
                    } else if ("3".equalsIgnoreCase(point[2])) {
                        cmd.setCommand("pressHome");
                    } else if ("82".equalsIgnoreCase(point[2])) {
                        cmd.setCommand("pressMenu");
                    } else if ("84".equalsIgnoreCase(point[2])) {
                        cmd.setCommand("pressSearch");
                    }
                }
                List<HashMap<String, Object>> params = new ArrayList<HashMap<String, Object>>();
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("user-deviceid", webSession.getAttributes().get("user-deviceid"));
                map.put("from", "web");
                map.put("sn", device.getSerialNo());
                params.add(map);
                cmd.setParams(params);
                String command = JacksonUtils.beanToJson(cmd);
                cmdClient.sendRequest(command);

                /***
                 * 浏览器通过websocket和Mina建立连接
                 */
                ImageRequest imageRequest = new ImageRequest();
                imageRequest.setData(command);
                imageRequest.setFrom("web");
                imageClient.sendRequest(imageRequest);
                logger.info("==>>CMD和Image client建立连接。");
            } else if ("STARTVIDEO".equalsIgnoreCase(payload)) {
                Command cmd = new Command();
                List<HashMap<String, Object>> params = new ArrayList<HashMap<String, Object>>();
                HashMap<String, Object> map = new HashMap<String, Object>();
                String udevid = webSession.getAttributes().get("user-deviceid").toString();
                map.put("user-deviceid", udevid);
                map.put("from", "web");
                map.put("sn", device.getSerialNo());
                String[] uid = udevid.split("_");
                String deviceId = uid[1];
                Device device2 = deviceMap.get(deviceId);
                String sno = null;
                if (device2 != null) {
                    sno = device2.getSerialNo();
                    device2.setStatus(StatusEnum.BUSY.getValue());
                    deviceService.update(device2);
                    logger.info("设置设备占用.");
                }
                map.put("sn", sno);
                params.add(map);
                cmd.setParams(params);
                cmd.setCommand(payload);
                String command = JacksonUtils.beanToJson(cmd);
                ImageRequest imageRequest = new ImageRequest();
                imageRequest.setData(command);
                imageRequest.setFrom("web");
                imageClient.sendRequest(imageRequest);
                cmdClient.sendRequest(command);
                logger.info("浏览器打开请求图像." + payload);
            } else if ("STOPVIDEO".equalsIgnoreCase(payload)) {
                Command cmd = new Command();
                List<HashMap<String, Object>> params = new ArrayList<HashMap<String, Object>>();
                HashMap<String, Object> map = new HashMap<String, Object>();
                String udevid = webSession.getAttributes().get("user-deviceid").toString();
                map.put("user-deviceid", udevid);
                map.put("from", "web");
                map.put("sn", device.getSerialNo());
                params.add(map);
                cmd.setParams(params);
                cmd.setCommand(payload);
                String command = JacksonUtils.beanToJson(cmd);
                ImageRequest imageRequest = new ImageRequest();
                imageRequest.setData(command);
                imageRequest.setFrom("web");
                cmdClient.sendRequest(command);
                logger.info("浏览器打开请求图像." + payload);
            }

        }

    }

    private String createCmd(String x, String y, Device device, String touch) {

        int mWidth = device.getWidth();
        int mHeight = device.getHeight();
        Command cmd = new Command();
        if (x == null || y == null) {
            return JacksonUtils.beanToJson(cmd);
        }
        cmd.setCommand(touch);

        List<HashMap<String, Object>> params = new ArrayList<HashMap<String, Object>>();
        HashMap<String, Object> map = new HashMap<String, Object>();

        int xx = (Integer.parseInt(x) * mWidth) / 337;
        int yy = (Integer.parseInt(y) * mHeight) / 600;

        map.put("x", xx);
        map.put("y", yy);

        map.put("user-deviceid", webSession.getAttributes().get("user-deviceid"));
        map.put("from", "adaptor");
        map.put("sn", device.getSerialNo());
        params.add(map);
        cmd.setParams(params);

        String command = JacksonUtils.beanToJson(cmd);

        logger.info("->>发送Json数据ToServer:" + command);

        return command;
    }

    @Override
    public void onCommand(String command) {
        System.out.println("on command." + command);
    }

    public Map<String, String> queryStringToMap(String queryString) {
        String[] q = queryString.split("&");
        Map<String, String> queryMap = new HashMap<String, String>();
        for (int i = 0; i < q.length; i++) {
            if (q[i] != null) {
                String[] m = q[i].split("=");
                if (m != null && m.length == 2) {
                    queryMap.put(m[0], m[1]);
                }
            }
        }
        return queryMap;
    }
}
