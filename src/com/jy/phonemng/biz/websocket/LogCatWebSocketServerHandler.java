package com.jy.phonemng.biz.websocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.BinaryWebSocketHandler;

/**
 * 
 * @author kejun.song
 * @version $Id: LogCatWebSocketServerHandler.java, v 0.1 2015��10��16�� ����9:27:26 kejun.song Exp $
 */
public class LogCatWebSocketServerHandler extends BinaryWebSocketHandler implements
                                                                        WebSocketHandler {

    protected Logger logger = LoggerFactory.getLogger(LogCatWebSocketServerHandler.class);

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status)
                                                                                   throws Exception {
        logger.info("->LogCatWebSocketServerHandler session closed.");
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        logger.info("LogCatWebSocketServerHandler-> session established.");
    }

    @Override
    public void handleMessage(WebSocketSession session, WebSocketMessage<?> message)
                                                                                    throws Exception {
        logger.info("LogCatWebSocketServerHandler->handlemessage");
    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable arg1) throws Exception {
        logger.info("##handleTransportError connection closed......");
        if (session != null && session.isOpen()) {
            //session.close(CloseStatus.SERVER_ERROR);
            logger.info("==== session closed by error.====");
        }

    }

    @Override
    public boolean supportsPartialMessages() {
        return false;
    }
}
