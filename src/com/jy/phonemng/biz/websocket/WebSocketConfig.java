/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package com.jy.phonemng.biz.websocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

/**
 * 
 * @author kejun.song
 * @version $Id: WebSocketConfig.java, v 0.1 2015��8��14�� ����7:59:30 kejun.song Exp $
 */
@Configuration
@EnableWebMvc
@EnableWebSocket
public class WebSocketConfig extends WebMvcConfigurerAdapter implements WebSocketConfigurer {

    protected Logger logger = LoggerFactory.getLogger(WebSocketConfig.class);

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        logger.info("#websocketע��:registerWebSocketHandlers registry=" + registry);

        registry.addHandler(imageWebSocketHandler(), "/PhoneMng").addInterceptors(
            new WebSocketHandshakeInterceptor());

        registry.addHandler(imageWebSocketHandler(), "/biz/device/wsServlet/getImage")
            .addInterceptors(new WebSocketHandshakeInterceptor());

        // registry.addHandler(imageWebSocketHandler(), "/biz/device/wsServlet/getLogCat")
        //     .addInterceptors(new WebSocketHandshakeInterceptor());

        registry.addHandler(imageWebSocketHandler(), "/biz/device/wsServlet/sendEvent")
            .addInterceptors(new WebSocketHandshakeInterceptor());//
    }

    @Bean
    public WebSocketHandler imageWebSocketHandler() {
        return new WebSocketServerHandler();
    }

    /* @Bean
     public WebSocketHandler logcatWebSocketHandler() {
         return new LogCatWebSocketServerHandler();
     }

     @Bean
     public WebSocketHandler sendEventWebSocketHandler() {
         return new SendEventWebSocketServerHandler();
     }*/

    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }
}
