/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package com.jy.phonemng.biz.dal.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.jy.phonemng.framework.annotation.EnableQueryCache;
import com.jy.phonemng.framework.base.BaseEntity;

/**
 * 
 * @author kejun.song
 * @version $Id: Log.java, v 0.1 2015��8��5�� ����7:43:57 kejun.song Exp $
 */
@Entity
@Table(name = "log")
@EnableQueryCache
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Log extends BaseEntity<Long> {

    /**  */
    private static final long serialVersionUID = 1L;

    @Column(name = "login_ip")
    private String            loginIp;

    @Column(name = "request_url")
    private String            requestUrl;
    @Column(name = "user_id")
    private Long              userId;

    //*************************//
    public String getLoginIp() {
        return loginIp;
    }

    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
