/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package com.jy.phonemng.biz.dal.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.jy.phonemng.framework.annotation.EnableQueryCache;
import com.jy.phonemng.framework.base.BaseEntity;

/**
 * 
 * @author kejun.song
 * @version $Id: Device.java, v 0.1 2015年8月11日 下午7:35:44 kejun.song Exp $
 */
@Entity
@Table(name = "device")
@EnableQueryCache
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Device extends BaseEntity<Long> {

    /**  */
    private static final long serialVersionUID = 1L;

    private String            location;             //机柜
    private String            brand;                //品牌

    @Column(name = "serial_no")
    private String            serialNo;             //序列号
    private String            os;                   //系统
    @Column(name = "os_version")
    private String            osVersion;            //os 系统版本
    private String            resolution;           //分辨率
    private String            status;               // B busy;F free; E error.

    @Column(name = "user_id")
    private Long              userId;
    @Column(name = "company_id")
    private String            companyId;            //出租公司；

    @Column(name = "hard_version")
    private String            hardVersion;
    @Column(name = "icon_name")
    private String            iconName;

    private int               width;
    private int               height;

    private String            mac;
    @Column(name = "sim_card")
    private String            simCard;
    private String            manufacture;

    @Column(name = "agent_mac")
    private String            agentMac;

    //////***********get and set ***************////////

    public String getAgentMac() {
        return agentMac;
    }

    public void setAgentMac(String agentMac) {
        this.agentMac = agentMac;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getSimCard() {
        return simCard;
    }

    public void setSimCard(String simCard) {
        this.simCard = simCard;
    }

    public String getManufacture() {
        return manufacture;
    }

    public void setManufacture(String manufacture) {
        this.manufacture = manufacture;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getHardVersion() {
        return hardVersion;
    }

    public void setHardVersion(String hardVersion) {
        this.hardVersion = hardVersion;
    }

    public String getIconName() {
        return iconName;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
