-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        5.6.20 - MySQL Community Server (GPL)
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 导出 phonemng 的数据库结构
CREATE DATABASE IF NOT EXISTS `phonemng` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `phonemng`;


-- 导出  表 phonemng.device 结构
CREATE TABLE IF NOT EXISTS `device` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '标示',
  `location` varchar(40) DEFAULT NULL COMMENT 'agent位置信息',
  `brand` varchar(30) DEFAULT NULL COMMENT '手机品牌',
  `serial_no` varchar(60) DEFAULT NULL COMMENT '设备序列号',
  `os` varchar(20) DEFAULT NULL COMMENT '操作系统',
  `os_version` varchar(15) DEFAULT NULL COMMENT '系统版本',
  `resolution` varchar(20) DEFAULT NULL COMMENT '分辨率',
  `status` varchar(15) DEFAULT NULL COMMENT '设备状态 B busy;F free; E error',
  `user_id` bigint(20) DEFAULT NULL COMMENT '设备归属用户',
  `company_id` bigint(20) DEFAULT NULL COMMENT '设备归属出租公司',
  `hard_version` varchar(20) DEFAULT NULL,
  `icon_name` varchar(20) DEFAULT NULL,
  `mac` varchar(40) DEFAULT NULL COMMENT 'mac',
  `agent_mac` varchar(40) DEFAULT NULL,
  `sim_card` varchar(40) DEFAULT NULL,
  `width` int(11) DEFAULT '0',
  `height` int(11) DEFAULT '0',
  `manufacture` varchar(40) DEFAULT NULL,
  `create_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='设备保存表。';

-- 正在导出表  phonemng.device 的数据：~13 rows (大约)
/*!40000 ALTER TABLE `device` DISABLE KEYS */;
INSERT INTO `device` (`id`, `location`, `brand`, `serial_no`, `os`, `os_version`, `resolution`, `status`, `user_id`, `company_id`, `hard_version`, `icon_name`, `mac`, `agent_mac`, `sim_card`, `width`, `height`, `manufacture`, `create_date`, `update_date`) VALUES
	(1, 'Shanghai', 'xiaomi', '23wewr112', 'Android', '4.1', '1280*1080', 'FREE', 1, 1, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, '2015-08-13 11:11:29', '2015-08-13 11:11:28'),
	(2, 'Shanghai', 'HUAWEI', '123qwea3442', 'Android', '4.4.1', '1280*1080', 'FREE', 1, 1, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, '2015-08-13 15:40:41', '2015-08-13 15:40:43'),
	(3, 'Shanghai', 'HTC', '123qwazx', 'Android', '4.6.2', '1280*890', 'FREE', 1, 1, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, '2015-08-13 16:35:01', '2015-08-13 16:35:03'),
	(4, 'Shanghai', 'K-Touch', 'kds23323', 'Android', '4.4.2', '890*680', 'FREE', 1, 1, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, '2015-08-13 16:35:12', '2015-08-13 16:35:12'),
	(5, 'BeiJing', 'LENOVO', '23432sdf', 'Android', '4.4.1', '1280*890', 'FREE', 1, 1, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, '2015-08-13 16:38:05', '2015-08-13 16:38:05'),
	(6, 'BeiJing', 'LENOVO', '32323dddsdsd', 'Android', '4.4.1', '1280*890', 'FREE', 1, 1, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, '2015-08-13 16:38:15', '2015-08-13 16:38:15'),
	(7, 'SHZ', 'LENOVO', '123333444444', 'Android', '4.4.1', '1280*890', 'FREE', 1, 1, 'L859', 'lenovo859', NULL, NULL, NULL, 0, 0, NULL, '2015-08-13 17:24:13', '2015-08-13 17:24:13'),
	(8, 'SHENZHEN', 'K-Touch', '12212', 'Android', '4.4.1', '1280*890', 'FREE', 1, 1, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, '2015-08-13 17:25:55', '2015-08-13 17:25:55'),
	(9, 'HANGZHOU', 'APPLE', '12kkj2kjkjkjds', 'IOS', '7.2', '1280*890', 'FREE', 1, 1, '5S', 'apple5s', NULL, NULL, NULL, 0, 0, NULL, '2015-08-13 17:27:03', '2015-08-13 17:27:03'),
	(10, 'HangZhou', 'Coolpad', '122121212121wqwq1', 'Android', '4..4.2', '1280*720', 'FREE', 1, 1, 'CL120S', 'coolpad120s', NULL, NULL, NULL, 0, 0, NULL, '2015-08-13 17:28:33', '2015-08-13 17:28:33'),
	(11, 'SuZhou', 'Coopad', 'coolpad123qweazx', 'Android', '4.4.1', '1280*720', 'FREE', 1, 1, 'CL5891', 'coolpad5891', NULL, NULL, NULL, 0, 0, NULL, '2015-08-13 17:29:41', '2015-08-13 17:29:41'),
	(12, 'ShangHai', 'Coolpad 8297', '0123456789ABCDEF', 'Android', '4.4.2', '720*1280', 'ONLINE', NULL, NULL, NULL, 'Coolpad 8297', '18:dc:56:f0:7e:2b', '08-00-27-00-60-B5', '', 720, 1280, 'Coolpad', '2015-09-08 20:25:19', '2015-09-08 20:25:28'),
	(13, 'ShangHai', 'MI 2', 'c0d999f1', 'Android', '4.1.1', '720*1280', 'ONLINE', NULL, NULL, NULL, 'MI 2', 'ac:f7:f3:ca:5c:4a', '08-00-27-00-60-B5', '', 720, 1280, 'Xiaomi', '2015-09-08 20:25:26', '2015-09-08 20:25:29');
/*!40000 ALTER TABLE `device` ENABLE KEYS */;


-- 导出  表 phonemng.log 结构
CREATE TABLE IF NOT EXISTS `log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `login_ip` varchar(16) DEFAULT NULL,
  `request_url` varchar(300) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `create_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='监控Log';

-- 正在导出表  phonemng.log 的数据：~11 rows (大约)
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
INSERT INTO `log` (`id`, `login_ip`, `request_url`, `user_id`, `create_date`, `update_date`) VALUES
	(1, '255.255.255.0', 'http://jy.com/biz', 1, '2015-08-06 10:28:50', '2015-08-06 10:28:51'),
	(2, '129.111.20.1', 'http://jy.com/query', 1, '2015-08-06 10:29:20', '2015-08-06 10:29:21'),
	(3, '255.255.255.0', 'http://jy.com/query', 1, '2015-08-06 10:58:41', '2015-08-06 10:58:40'),
	(4, '255.255.255.0', 'http://jy.com/query', 1, '2015-08-06 10:58:42', '2015-08-06 10:58:43'),
	(5, '255.255.255.0', 'http://jy.com/query', 1, '2015-08-06 10:58:45', '2015-08-06 10:58:44'),
	(6, '255.255.255.0', 'http://jy.com/query', 1, '2015-08-06 10:58:46', '2015-08-06 10:58:49'),
	(7, '255.255.255.0', 'http://jy.com/query', 1, '2015-08-06 10:58:47', '2015-08-06 10:58:50'),
	(8, '255.255.255.0', 'http://jy.com/query', 1, '2015-08-06 10:58:52', '2015-08-06 10:58:51'),
	(9, '255.255.255.0', 'http://jy.com/query', 1, '2015-08-06 10:58:53', '2015-08-06 10:58:54'),
	(10, '255.255.255.0', 'http://jy.com/query', 1, '2015-08-06 10:58:36', '2015-08-06 10:58:37'),
	(11, '255.255.255.0', 'http://jy.com/query', 1, '2015-08-06 10:58:35', '2015-08-06 10:58:39');
/*!40000 ALTER TABLE `log` ENABLE KEYS */;


-- 导出  表 phonemng.user 结构
CREATE TABLE IF NOT EXISTS `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) DEFAULT NULL,
  `mobile_phone_number` varchar(20) DEFAULT NULL,
  `user_name` varchar(40) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `deleted` varchar(4) DEFAULT NULL,
  `salt` varchar(11) DEFAULT NULL,
  `nick_name` varchar(40) DEFAULT NULL,
  `expired_date` datetime DEFAULT NULL,
  `last_login_date` datetime DEFAULT NULL,
  `last_login_ip` varchar(50) DEFAULT NULL,
  `admin` varchar(4) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `create_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='保存用户帐户信息';

-- 正在导出表  phonemng.user 的数据：~1 rows (大约)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `email`, `mobile_phone_number`, `user_name`, `password`, `deleted`, `salt`, `nick_name`, `expired_date`, `last_login_date`, `last_login_ip`, `admin`, `status`, `create_date`, `update_date`) VALUES
	(1, 'admin@eamil.com', '13817676816', 'admin', 'e34d5147bd3be91c8dd9f7e6e0075835', '0', '4waxiBK2YX', 'admin', '2016-08-05 00:00:00', '2015-08-05 00:00:00', NULL, '1', 'normal', '2015-08-05 00:00:00', '2015-08-05 00:00:00'),
	(2, 'hezh@163.com', '13817676816', 'hezheng', 'daf070660347511bcefebf2e3df528ac', '0', 'mcOzG3trDc', 'hezheng', '2017-09-07 19:04:02', NULL, NULL, '1', 'normal', '2015-09-07 19:03:29', '2015-09-07 19:03:29');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
